package tkn.dev.tknservices.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.manager.ItServicesListManager
import tkn.dev.tknservices.view.ItServicesListItem

/**
 * Created by lifetofree on 12/1/17.
 */
class ItServicesListAdapter : BaseAdapter() {
    override fun getCount(): Int {
        if (ItServicesListManager.getInstance().dao == null)
            return 0
        if (ItServicesListManager.getInstance().dao.BoxUserRequest == null)
            return 0

        return ItServicesListManager.getInstance().dao.BoxUserRequest!!.size
    }

    override fun getItem(position: Int): Any? {
        return ItServicesListManager.getInstance().dao.BoxUserRequest!![position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {

        var item: ItServicesListItem
        if (convertView != null) {
            item = convertView as ItServicesListItem
        } else {
            item = ItServicesListItem(parent.context)
        }

        var dao = getItem(position) as UserRequestList
        item.setCaseText_doccode(dao.doccode.toString())
        item.setCaseText_CreateDateUser(dao.createdateuser.toString())
        item.setCaseText_TimeCreateJob(dao.timecreatejob.toString())
        item.setCaseText_EmpName(dao.empname.toString())
        item.setCaseText_RDeptName(dao.rdeptname.toString())
        item.setCaseText_LocName(dao.locname.toString())
        item.setCaseText_detailUser(dao.detailuser.toString())
        item.setCaseText_Status(dao.staname.toString())

        return item
    }
}