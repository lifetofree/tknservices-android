package tkn.dev.tknservices.dao

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


data class DataSupportIT(

        @SerializedName("ReturnMsg") var ReturnMsg: String? = "",
        @SerializedName("ReturnCode") var ReturnCode: Int? = 0,
        @SerializedName("BoxUserRequest") var BoxUserRequest: List<UserRequestList>? = null,
        @SerializedName("BoxPOSList") var BoxPOSList: List<POSList>? = null,

        @SerializedName("employee_list") var employee_list: List<data_employeelist>? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.createTypedArrayList(UserRequestList),
            parcel.createTypedArrayList(POSList),
            parcel.createTypedArrayList(data_employeelist)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ReturnMsg)
        parcel.writeValue(ReturnCode)
        parcel.writeTypedList(BoxUserRequest)
        parcel.writeTypedList(BoxPOSList)
        parcel.writeTypedList(employee_list)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DataSupportIT> {
        override fun createFromParcel(parcel: Parcel): DataSupportIT {
            return DataSupportIT(parcel)
        }

        override fun newArray(size: Int): Array<DataSupportIT?> {
            return arrayOfNulls(size)
        }
    }
}

data class data_employeelist(
        @SerializedName("org_idx") var org_idx: Int? = 0,
        @SerializedName("LocName") var LocName: String? = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(org_idx)
        parcel.writeString(LocName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<data_employeelist> {
        override fun createFromParcel(parcel: Parcel): data_employeelist {
            return data_employeelist(parcel)
        }

        override fun newArray(size: Int): Array<data_employeelist?> {
            return arrayOfNulls(size)
        }
    }
}

data class UserRequestList(
        @SerializedName("EmailIDX") var emailidx: Int? = 0,
        @SerializedName("URQIDX") var urqidx: Int? = 0,
        //@SerializedName("MS1IDX") var ms1idx: Int? = 0,
        @SerializedName("MS2IDX") var ms2idx: Int? = 0,
        @SerializedName("RecieveIDX") var recieveidx: Int? = 0,
        @SerializedName("NCEmpIDX") var ncempidx: Int? = 0,
        @SerializedName("MS3IDX") var ms3idx: Int? = 0,
        @SerializedName("MS4IDX") var ms4idx: Int? = 0,
        @SerializedName("MS5IDX") var ms5idx: Int? = 0,
        @SerializedName("unidx") var unidx: Int? = 0,
        @SerializedName("acidx") var acidx: Int? = 0,
        @SerializedName("Priority_name") var Priority_name: String? = "",
        @SerializedName("PIDX") var pidx: Int? = 0,
        @SerializedName("CStatus") var cstatus: Int? = 0,
        @SerializedName("IDXCMSAP") var idxcmsap: Int? = 0,
        @SerializedName("EmpIDX") var empidx: Int? = 0,
        @SerializedName("EmpIDX_add") var empidx_add: Int? = 0,
        @SerializedName("SysIDX_add") var sysidx_add: Int? = 0,
        @SerializedName("CIT1IDX") var cit1idx: Int? = 0,
        @SerializedName("CIT2IDX") var cit2idx: Int? = 0,
        @SerializedName("CIT3IDX") var cit3idx: Int? = 0,
        @SerializedName("CIT4IDX") var cit4idx: Int? = 0,
        @SerializedName("Link_IT") var link_it: String? = "",
        @SerializedName("u0_code") var u0_code: String? = "",
        @SerializedName("POS1IDX") var p0s1idx: Int? = 0,
        @SerializedName("POS2IDX") var p0s2idx: Int? = 0,
        @SerializedName("POS3IDX") var p0s3idx: Int? = 0,
        @SerializedName("POS4IDX") var p0s4idx: Int? = 0,
        @SerializedName("Saplink") var saplink: String? = "",
        @SerializedName("ITlink") var itlink: String? = "",
        @SerializedName("Name_Code1") var name_code1: String? = "",
        @SerializedName("Name_Code2") var name_code2: String? = "",
        @SerializedName("Name_Code3") var name_code3: String? = "",
        @SerializedName("Name_Code4") var name_code4: String? = "",
        @SerializedName("Name_Code5") var name_code5: String? = "",
        @SerializedName("Name_Code6") var name_code6: String? = "",
        @SerializedName("Name_Code7") var name_code7: String? = "",
        @SerializedName("Name_Code8") var name_code8: String? = "",
        @SerializedName("ChkPlace") var chkplace: String? = "",
        @SerializedName("ChkAdaccept") var chkadaccept: String? = "",
        @SerializedName("ChkAddoing") var chkaddoing: String? = "",
        @SerializedName("DC_YEAR") var dc_year: String? = "",
        @SerializedName("DC_Mount") var dc_mount: String? = "",
        @SerializedName("PIDX_Add") var pidx_add: Int? = 0,
        @SerializedName("DocCode") var doccode: String? = "",
        @SerializedName("EmpCode") var empcode: String? = "",
        @SerializedName("FullNameTH") var fullnameth: String? = "",
        @SerializedName("Statusans") var statusans: String? = "",
        @SerializedName("NameFull") var namefull: String? = "",
        @SerializedName("NameCut") var namecut: String? = "",
        @SerializedName("CommentAuto") var commentauto: String? = "",
        @SerializedName("CodeCut") var codecut: String? = "",
        @SerializedName("OLIDX") var olidx: Int? = 0,
        @SerializedName("RSecID") var rsecid: Int? = 0,
        @SerializedName("IFDate") var ifdate: Int? = 0,
        @SerializedName("r0idx") var r0idx: Int? = 0,
        @SerializedName("sumtime") var sumtime: String? = "",
        @SerializedName("nonkpi") var nonkpi: String? = "",
        @SerializedName("alltime") var alltime: String? = "",
        @SerializedName("EmpName") var empname: String? = "",
        @SerializedName("SecNameTH") var secnameth: String? = "",
        @SerializedName("RDeptName") var rdeptname: String? = "",
        @SerializedName("NameEqDeviceETC") var nameeqdeviceetc: String? = "",
        @SerializedName("RPosIDX_J") var rposidx_j: Int? = 0,
        @SerializedName("PosNameTH") var posnameth: String? = "",
        @SerializedName("RDeptID") var rdeptid: Int? = 0,
        @SerializedName("RDeptIDX") var rdeptidx: Int? = 0,
        @SerializedName("DeptNameTH") var deptnameth: String? = "",
        @SerializedName("LocIDX") var locidx: Int? = 0,
        @SerializedName("LocName") var locname: String? = "",
        @SerializedName("IPAddress") var ipaddress: String? = "",
        @SerializedName("OrgIDX") var orgidx: Int? = 0,
        @SerializedName("OrgNameTH") var orgnameth: String? = "",
        @SerializedName("SysIDX") var sysidx: Int? = 0,
        @SerializedName("SysName") var sysname: String? = "",
        @SerializedName("SysName1") var sysname1: String? = "",
        @SerializedName("Email") var email: String? = "",
        @SerializedName("MobileNo") var mobileno: String? = "",
        @SerializedName("TelETC") var teletc: String? = "",
        @SerializedName("EmailETC") var emailetc: String? = "",
        @SerializedName("CostIDX") var costidx: Int? = 0,
        @SerializedName("CostNo") var costno: String? = "",
        @SerializedName("CostName") var costname: String? = "",
        @SerializedName("CheckRemote") var checkremote: Int? = 0,
        @SerializedName("RemoteIDX") var remoteidx: Int? = 0,
        @SerializedName("RemoteName") var remotename: String? = "",
        @SerializedName("UserIDRemote") var useridremote: String? = "",
        @SerializedName("PasswordRemote") var passwordremote: String? = "",
        @SerializedName("AdminIDX") var adminidx: Int? = 0,
        @SerializedName("AdminName") var adminname: String? = "",
        @SerializedName("TimegetJob") var timegetjob: String? = "",
//        @SerializedName("DategetJob") var dategetjob: String? = "",
        @SerializedName("DategetJobSearch") var dategetjobsearch: String? = "",
        @SerializedName("TimecloseJob") var timeclosejob: String? = "",
        @SerializedName("CreateDateUser") var createdateuser: String? = "",
        @SerializedName("TimeCreateJob") var timecreatejob: String? = "",
        @SerializedName("OtherName") var othername: String? = "",
        @SerializedName("DatecloseJob1") var dateclosejob1: String? = "",
        @SerializedName("DatecloseJob") var dateclosejob: String? = "",
        @SerializedName("DatecloseJobSearch") var dateclosejobsearch: String? = "",
        @SerializedName("DateReciveJobFirst") var daterecivejobfirst: String? = "",
        @SerializedName("TimeReciveJobFirst") var timerecivejobfirst: String? = "",
        @SerializedName("AllContactAdmin") var allcontactadmin: String? = "",
        @SerializedName("Comment") var comment: String? = "",
        @SerializedName("AdminDoingIDX") var admindoingidx: Int? = 0,
        @SerializedName("AdminDoingName") var admindoingname: String? = "",
        @SerializedName("CommentAMDoing") var commentamdoing: String? = "",
        @SerializedName("FileAMDoing") var fileamdoing: Int? = 0,
        @SerializedName("DateCloseJob") var dateClosejob: String? = "",
        @SerializedName("CCAIDX") var ccaidx: Int? = 0,
        @SerializedName("SysIDXNoti") var sysidxnoti: Int? = 0,
        @SerializedName("AddSort") var addsort: String? = "",
        @SerializedName("CaseMean") var casemean: String? = "",
        @SerializedName("Abbreviation") var abbreviation: String? = "",
        @SerializedName("addPlus") var addplus: String? = "",
        @SerializedName("NameSys") var namesys: String? = "",
        @SerializedName("StaIDX") var staidx: Int? = 0,
        @SerializedName("amount") var amount: Int? = 0,
        @SerializedName("CountStatus") var countstatus: Int? = 0,
        @SerializedName("StaName") var staname: String? = "",
        @SerializedName("FileUser") var fileuser: Int? = 0,
        @SerializedName("detailUser") var detailuser: String? = "",
        @SerializedName("UserName") var username: String? = "",
        @SerializedName("UserLogonName") var userlogonname: String? = "",
        @SerializedName("TransactionCode") var transactioncode: String? = "",
        @SerializedName("DeviceIDX") var deviceidx: Int? = 0,
        @SerializedName("DeviceIDX_IT") var deviceidx_it: Int? = 0,
        @SerializedName("DeviceCode_IT") var devicecode_it: String? = "",
        @SerializedName("DeviceETC_IT") var deviceetc_it: String? = "",
        @SerializedName("EmailIDX_LN") var emailidx_ln: String? = "",
        @SerializedName("EmailNameETC_LN") var emailnameetc_ln: String? = "",
        @SerializedName("RepairEmail") var repairemail: String? = "",
        @SerializedName("DeviceIDX_LN") var deviceidx_ln: Int? = 0,
        @SerializedName("DeviceCode_LN") var devicecode_ln: String? = "",
        @SerializedName("DeviceETC_LN") var deviceetc_ln: String? = "",
        @SerializedName("TypeDeviceIDX") var typedeviceidx: Int? = 0,
        @SerializedName("TypeNameEquip_IT") var typenameequip_it: String? = "",
        @SerializedName("EndWork") var endwork: String? = "",
        @SerializedName("SearchSystem") var searchsystem: Int? = 0,
        @SerializedName("SearchLocation") var searchlocation: Int? = 0,
        @SerializedName("SearchFirstName") var searchfirstname: String? = "",
        @SerializedName("SearchLastName") var searchlastname: String? = "",
        @SerializedName("SearchOrganize") var searchorganize: Int? = 0,
        @SerializedName("SearchDept") var searchdept: Int? = 0,
        @SerializedName("SearchSatatus") var searchsatatus: Int? = 0,
        @SerializedName("IFSearch") var ifsearch: Int? = 0,
        @SerializedName("IFSearchragbetween") var ifsearchragbetween: Int? = 0,
        @SerializedName("SearchDate") var searchdate: String? = "",
        @SerializedName("Code") var code: String? = "",
        @SerializedName("CountCase") var countcase: Int? = 0,
        @SerializedName("CountEmail") var countemail: Int? = 0,
        @SerializedName("CountSAP") var countsap: Int? = 0,
        @SerializedName("CountIT") var countit: Int? = 0,
        @SerializedName("CountComputer") var countcomputer: Int? = 0,
        @SerializedName("CountMonitor") var countmonitor: Int? = 0,
        @SerializedName("CountNotebook") var countnotebook: Int? = 0,
        @SerializedName("CountPrinter") var countprinter: Int? = 0,
        @SerializedName("CountInternet") var countinternet: Int? = 0,
        @SerializedName("CountSoftware") var countsoftware: Int? = 0,
        @SerializedName("CountOther") var countother: Int? = 0,
        @SerializedName("ManHours") var manhours: Int? = 0,
        @SerializedName("Link") var link: String? = "",
        @SerializedName("SapMsg") var sapmsg: String? = "",
        @SerializedName("CMIDX") var cmidx: Int? = 0,
        @SerializedName("node_status") var node_status: Int? = 0,
        @SerializedName("Name1") var name1: String? = "",
        @SerializedName("Name2") var name2: String? = "",
        @SerializedName("Name3") var name3: String? = "",
        @SerializedName("Name4") var name4: String? = "",
        @SerializedName("Name5") var name5: String? = "",
        @SerializedName("Name6") var name6: String? = "",
        @SerializedName("Name7") var name7: String? = "",
        @SerializedName("Name8") var name8: String? = "",
        @SerializedName("Name9") var name9: String? = "",
        @SerializedName("Name10") var name10: String? = "",
        @SerializedName("Name11") var name11: String? = "",
        @SerializedName("Name12") var name12: String? = "",
        @SerializedName("Name13") var name13: String? = "",
        @SerializedName("CommentAutoIT") var commentautoit: String? = "",
        @SerializedName("CDate") var cdate: String? = "",
        @SerializedName("CTime") var ctime: String? = "",
        @SerializedName("ISO_User") var iso_user: String? = "",
        @SerializedName("ChkOrg") var chkorg: String? = "",
        @SerializedName("ChkOrgGM") var chkorggm: String? = "",
        @SerializedName("ChkSys") var chksys: String? = "",
        @SerializedName("SysIDXGM") var sysidxgm: Int? = 0,
        @SerializedName("Downtime_KPI") var downtime_kpi: String? = "",
        @SerializedName("Downtim_NONKPI") var downtim_nonkpi: String? = "",
        @SerializedName("ALLTIME") var aLLTIME: String? = "",
        @SerializedName("countit1id") var countit1id: String? = "",
        @SerializedName("countit2id") var countit2id: String? = "",
        @SerializedName("countit3id") var countit3id: String? = "",
        @SerializedName("countit4id") var countit4id: String? = "",
        @SerializedName("Name") var name: String? = "",
        @SerializedName("cc") var cc: Int? = 0,
        @SerializedName("emp_name_th") var emp_name_th: String? = "",
        @SerializedName("DeviceETC") var deviceetc: String? = ""

) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(emailidx)
        parcel.writeValue(urqidx)
        parcel.writeValue(ms2idx)
        parcel.writeValue(recieveidx)
        parcel.writeValue(ncempidx)
        parcel.writeValue(ms3idx)
        parcel.writeValue(ms4idx)
        parcel.writeValue(ms5idx)
        parcel.writeValue(unidx)
        parcel.writeValue(acidx)
        parcel.writeString(Priority_name)
        parcel.writeValue(pidx)
        parcel.writeValue(cstatus)
        parcel.writeValue(idxcmsap)
        parcel.writeValue(empidx)
        parcel.writeValue(empidx_add)
        parcel.writeValue(sysidx_add)
        parcel.writeValue(cit1idx)
        parcel.writeValue(cit2idx)
        parcel.writeValue(cit3idx)
        parcel.writeValue(cit4idx)
        parcel.writeString(link_it)
        parcel.writeString(u0_code)
        parcel.writeValue(p0s1idx)
        parcel.writeValue(p0s2idx)
        parcel.writeValue(p0s3idx)
        parcel.writeValue(p0s4idx)
        parcel.writeString(saplink)
        parcel.writeString(itlink)
        parcel.writeString(name_code1)
        parcel.writeString(name_code2)
        parcel.writeString(name_code3)
        parcel.writeString(name_code4)
        parcel.writeString(name_code5)
        parcel.writeString(name_code6)
        parcel.writeString(name_code7)
        parcel.writeString(name_code8)
        parcel.writeString(chkplace)
        parcel.writeString(chkadaccept)
        parcel.writeString(chkaddoing)
        parcel.writeString(dc_year)
        parcel.writeString(dc_mount)
        parcel.writeValue(pidx_add)
        parcel.writeString(doccode)
        parcel.writeString(empcode)
        parcel.writeString(fullnameth)
        parcel.writeString(statusans)
        parcel.writeString(namefull)
        parcel.writeString(namecut)
        parcel.writeString(commentauto)
        parcel.writeString(codecut)
        parcel.writeValue(olidx)
        parcel.writeValue(rsecid)
        parcel.writeValue(ifdate)
        parcel.writeValue(r0idx)
        parcel.writeString(sumtime)
        parcel.writeString(nonkpi)
        parcel.writeString(alltime)
        parcel.writeString(empname)
        parcel.writeString(secnameth)
        parcel.writeString(rdeptname)
        parcel.writeString(nameeqdeviceetc)
        parcel.writeValue(rposidx_j)
        parcel.writeString(posnameth)
        parcel.writeValue(rdeptid)
        parcel.writeValue(rdeptidx)
        parcel.writeString(deptnameth)
        parcel.writeValue(locidx)
        parcel.writeString(locname)
        parcel.writeString(ipaddress)
        parcel.writeValue(orgidx)
        parcel.writeString(orgnameth)
        parcel.writeValue(sysidx)
        parcel.writeString(sysname)
        parcel.writeString(sysname1)
        parcel.writeString(email)
        parcel.writeString(mobileno)
        parcel.writeString(teletc)
        parcel.writeString(emailetc)
        parcel.writeValue(costidx)
        parcel.writeString(costno)
        parcel.writeString(costname)
        parcel.writeValue(checkremote)
        parcel.writeValue(remoteidx)
        parcel.writeString(remotename)
        parcel.writeString(useridremote)
        parcel.writeString(passwordremote)
        parcel.writeValue(adminidx)
        parcel.writeString(adminname)
        parcel.writeString(timegetjob)
        parcel.writeString(dategetjobsearch)
        parcel.writeString(timeclosejob)
        parcel.writeString(createdateuser)
        parcel.writeString(timecreatejob)
        parcel.writeString(othername)
        parcel.writeString(dateclosejob1)
        parcel.writeString(dateclosejob)
        parcel.writeString(dateclosejobsearch)
        parcel.writeString(daterecivejobfirst)
        parcel.writeString(timerecivejobfirst)
        parcel.writeString(allcontactadmin)
        parcel.writeString(comment)
        parcel.writeValue(admindoingidx)
        parcel.writeString(admindoingname)
        parcel.writeString(commentamdoing)
        parcel.writeValue(fileamdoing)
        parcel.writeString(dateClosejob)
        parcel.writeValue(ccaidx)
        parcel.writeValue(sysidxnoti)
        parcel.writeString(addsort)
        parcel.writeString(casemean)
        parcel.writeString(abbreviation)
        parcel.writeString(addplus)
        parcel.writeString(namesys)
        parcel.writeValue(staidx)
        parcel.writeValue(amount)
        parcel.writeValue(countstatus)
        parcel.writeString(staname)
        parcel.writeValue(fileuser)
        parcel.writeString(detailuser)
        parcel.writeString(username)
        parcel.writeString(userlogonname)
        parcel.writeString(transactioncode)
        parcel.writeValue(deviceidx)
        parcel.writeValue(deviceidx_it)
        parcel.writeString(devicecode_it)
        parcel.writeString(deviceetc_it)
        parcel.writeString(emailidx_ln)
        parcel.writeString(emailnameetc_ln)
        parcel.writeString(repairemail)
        parcel.writeValue(deviceidx_ln)
        parcel.writeString(devicecode_ln)
        parcel.writeString(deviceetc_ln)
        parcel.writeValue(typedeviceidx)
        parcel.writeString(typenameequip_it)
        parcel.writeString(endwork)
        parcel.writeValue(searchsystem)
        parcel.writeValue(searchlocation)
        parcel.writeString(searchfirstname)
        parcel.writeString(searchlastname)
        parcel.writeValue(searchorganize)
        parcel.writeValue(searchdept)
        parcel.writeValue(searchsatatus)
        parcel.writeValue(ifsearch)
        parcel.writeValue(ifsearchragbetween)
        parcel.writeString(searchdate)
        parcel.writeString(code)
        parcel.writeValue(countcase)
        parcel.writeValue(countemail)
        parcel.writeValue(countsap)
        parcel.writeValue(countit)
        parcel.writeValue(countcomputer)
        parcel.writeValue(countmonitor)
        parcel.writeValue(countnotebook)
        parcel.writeValue(countprinter)
        parcel.writeValue(countinternet)
        parcel.writeValue(countsoftware)
        parcel.writeValue(countother)
        parcel.writeValue(manhours)
        parcel.writeString(link)
        parcel.writeString(sapmsg)
        parcel.writeValue(cmidx)
        parcel.writeValue(node_status)
        parcel.writeString(name1)
        parcel.writeString(name2)
        parcel.writeString(name3)
        parcel.writeString(name4)
        parcel.writeString(name5)
        parcel.writeString(name6)
        parcel.writeString(name7)
        parcel.writeString(name8)
        parcel.writeString(name9)
        parcel.writeString(name10)
        parcel.writeString(name11)
        parcel.writeString(name12)
        parcel.writeString(name13)
        parcel.writeString(commentautoit)
        parcel.writeString(cdate)
        parcel.writeString(ctime)
        parcel.writeString(iso_user)
        parcel.writeString(chkorg)
        parcel.writeString(chkorggm)
        parcel.writeString(chksys)
        parcel.writeValue(sysidxgm)
        parcel.writeString(downtime_kpi)
        parcel.writeString(downtim_nonkpi)
        parcel.writeString(aLLTIME)
        parcel.writeString(countit1id)
        parcel.writeString(countit2id)
        parcel.writeString(countit3id)
        parcel.writeString(countit4id)
        parcel.writeString(name)
        parcel.writeValue(cc)
        parcel.writeString(emp_name_th)
        parcel.writeString(deviceetc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserRequestList> {
        override fun createFromParcel(parcel: Parcel): UserRequestList {
            return UserRequestList(parcel)
        }

        override fun newArray(size: Int): Array<UserRequestList?> {
            return arrayOfNulls(size)
        }
    }

}

data class POSList(
        //@SerializedName("POS1IDX") var pos1idx: Int? = 0,
        @SerializedName("POS2IDX") var pos2idx: Int? = 0,
        @SerializedName("POS3IDX") var pos3idx: Int? = 0,
        @SerializedName("POS4IDX") var pos4idx: Int? = 0,

        @SerializedName("POS1_Name") var pos1_name: String? = "",
        @SerializedName("POS2_Name") var pos2_name: String? = "",
        @SerializedName("POS3_Name") var pos3_name: String? = "",
        @SerializedName("POS4_Name") var pos4_name: String? = "",
        @SerializedName("POS1_Code") var pos1_code: String? = "",
        @SerializedName("POS2_Code") var pos2_code: String? = "",
        @SerializedName("POS3_Code") var pos3_code: String? = "",
        @SerializedName("POS4_Code") var pos4_code: String? = "",

        @SerializedName("CEmpIDX") var cempidx: Int? = 0,
        @SerializedName("POS1Status") var pos1status: Int? = 0,
        @SerializedName("POS2Status") var pos2status: Int? = 0,
        @SerializedName("POS3Status") var pos3status: Int? = 0,
        @SerializedName("POS4Status") var pos4status: Int? = 0,
        @SerializedName("POSStatusDetail") var posstatusdetail: String? = "",
        @SerializedName("POSIDX") var posidx: Int? = 0,
        @SerializedName("POSStatus") var posstatus: Int? = 0,
        @SerializedName("Name_Code1") var name_code1: String? = "",
        @SerializedName("Name_Code2") var name_code2: String? = "",
        @SerializedName("Name_Code3") var name_code3: String? = "",
        @SerializedName("Name_Code4") var name_code4: String? = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(pos2idx)
        parcel.writeValue(pos3idx)
        parcel.writeValue(pos4idx)
        parcel.writeString(pos1_name)
        parcel.writeString(pos2_name)
        parcel.writeString(pos3_name)
        parcel.writeString(pos4_name)
        parcel.writeString(pos1_code)
        parcel.writeString(pos2_code)
        parcel.writeString(pos3_code)
        parcel.writeString(pos4_code)
        parcel.writeValue(cempidx)
        parcel.writeValue(pos1status)
        parcel.writeValue(pos2status)
        parcel.writeValue(pos3status)
        parcel.writeValue(pos4status)
        parcel.writeString(posstatusdetail)
        parcel.writeValue(posidx)
        parcel.writeValue(posstatus)
        parcel.writeString(name_code1)
        parcel.writeString(name_code2)
        parcel.writeString(name_code3)
        parcel.writeString(name_code4)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<POSList> {
        override fun createFromParcel(parcel: Parcel): POSList {
            return POSList(parcel)
        }

        override fun newArray(size: Int): Array<POSList?> {
            return arrayOfNulls(size)
        }
    }
}