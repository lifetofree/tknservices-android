package tkn.dev.tknservices.dao

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


data class DataEmployee(
        @SerializedName("return_code") var returnCode: Int? = 0,
        @SerializedName("return_msg") var returnMsg: String? = "",
        @SerializedName("employee_list") var employeeList: List<EmployeeDetail>? = null,

        @SerializedName("organization_list") var organizationList: List<OrganizationDetail>? = null,
        @SerializedName("department_list") var departmentList: List<DepartmentDetail>? = null,
        @SerializedName("section_list") var sectionList: List<SectionDetail>? = null,
        @SerializedName("position_list") var positionList: List<PositionDetail>? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.createTypedArrayList(EmployeeDetail),
            parcel.createTypedArrayList(OrganizationDetail),
            parcel.createTypedArrayList(DepartmentDetail),
            parcel.createTypedArrayList(SectionDetail),
            parcel.createTypedArrayList(PositionDetail)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(returnCode)
        parcel.writeString(returnMsg)
        parcel.writeTypedList(employeeList)
        parcel.writeTypedList(organizationList)
        parcel.writeTypedList(departmentList)
        parcel.writeTypedList(sectionList)
        parcel.writeTypedList(positionList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DataEmployee> {
        override fun createFromParcel(parcel: Parcel): DataEmployee {
            return DataEmployee(parcel)
        }

        override fun newArray(size: Int): Array<DataEmployee?> {
            return arrayOfNulls(size)
        }
    }
}

data class EmployeeDetail(
        @SerializedName("emp_idx") var empIdx: Int? = 0,

        @SerializedName("emp_code") var empCode: String? = "",
        @SerializedName("emp_password") var empPassword: String? = "",

        @SerializedName("emp_name_en") var empNameEn: String? = "",
        @SerializedName("emp_name_th") var empNameTh: String? = "",

        @SerializedName("emp_firstname_en") var empFirstnameEn: String? = "",
        @SerializedName("emp_lastname_en") var empLastnameEn: String? = "",
        @SerializedName("emp_firstname_th") var empFirstnameTh: String? = "",
        @SerializedName("emp_lastname_th") var empLastnameTh: String? = "",

        @SerializedName("emp_nickname_en") var empNicknameEn: String? = "",
        @SerializedName("emp_nickname_th") var empNicknameTh: String? = "",

        @SerializedName("sex_idx") var sexIdx: Int? = 0,
        @SerializedName("sex_name_en") var sexNameEn: String? = "",
        @SerializedName("sex_name_th") var sexNameTh: String? = "",

        @SerializedName("emp_phone_no") var empPhoneNo: String? = "",
        @SerializedName("emp_mobile_no") var empMobileNo: String? = "",

        @SerializedName("emp_email") var empEmail: String? = "",

        @SerializedName("emp_birthday") var empBirthday: String? = "",

        @SerializedName("emp_type_idx") var empTypeIdx: Int? = 0,
        @SerializedName("emp_type_name") var empTypeName: String? = "",

        @SerializedName("org_idx") var orgIdx: Int? = 0,
        @SerializedName("rdept_idx") var rdeptIdx: Int? = 0,
        @SerializedName("rsec_idx") var rsecIdx: Int? = 0,
        @SerializedName("rpos_idx") var rposIdx: Int? = 0,

        @SerializedName("r0idx") var r0Idx: Int? = 0,
        @SerializedName("LocName") var locName: String? = "",

        @SerializedName("org_name_en") var orgNameEn: String? = "",
        @SerializedName("dept_name_en") var deptNameEn: String? = "",
        @SerializedName("sec_name_en") var secNameEn: String? = "",
        @SerializedName("pos_name_en") var posNameEn: String? = "",

        @SerializedName("org_name_th") var orgNameTh: String? = "",
        @SerializedName("dept_name_th") var deptNameTh: String? = "",
        @SerializedName("sec_name_th") var secNameTh: String? = "",
        @SerializedName("pos_name_th") var posNameTh: String? = "",

        @SerializedName("jobgrade_idx") var jobGradeIdx: Int? = 0,
        @SerializedName("jobgrade_level") var jobGradeLevel: Int? = 0,

        @SerializedName("costcenter_idx") var costCenterIdx: Int? = 0,
        @SerializedName("costcenter_no") var costCenterNo: String? = "",
        @SerializedName("costcenter_name") var costCenterName: String? = "",

        @SerializedName("emp_start_date") var empStartDate: String? = "",
        @SerializedName("emp_probation_status") var empProbationStatus: Int? = 0,
        @SerializedName("emp_probation_date") var empProbationDate: String? = "",

        @SerializedName("emp_createdate") var empCreatedate: String? = "",
        @SerializedName("emp_updatedate") var empUpdatedate: String? = "",
        @SerializedName("emp_status") var empStatus: Int? = 0,

        @SerializedName("new_password") var nawPassword: String? = "",
        @SerializedName("type_reset") var typeReset: Int? = 0,

        @SerializedName("emp_idx_approve1") var empIdxApprove1: Int? = 0,
        @SerializedName("emp_idx_approve2") var empIdxApprove2: Int? = 0,
        @SerializedName("emp_approve1") var empApprove1: String? = "",
        @SerializedName("emp_approve2") var empApprove2: String? = "",

        @SerializedName("ip_address") var ipAddress: String? = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(empIdx)
        parcel.writeString(empCode)
        parcel.writeString(empPassword)
        parcel.writeString(empNameEn)
        parcel.writeString(empNameTh)
        parcel.writeString(empFirstnameEn)
        parcel.writeString(empLastnameEn)
        parcel.writeString(empFirstnameTh)
        parcel.writeString(empLastnameTh)
        parcel.writeString(empNicknameEn)
        parcel.writeString(empNicknameTh)
        parcel.writeValue(sexIdx)
        parcel.writeString(sexNameEn)
        parcel.writeString(sexNameTh)
        parcel.writeString(empPhoneNo)
        parcel.writeString(empMobileNo)
        parcel.writeString(empEmail)
        parcel.writeString(empBirthday)
        parcel.writeValue(empTypeIdx)
        parcel.writeString(empTypeName)
        parcel.writeValue(orgIdx)
        parcel.writeValue(rdeptIdx)
        parcel.writeValue(rsecIdx)
        parcel.writeValue(rposIdx)
        parcel.writeValue(r0Idx)
        parcel.writeString(locName)
        parcel.writeString(orgNameEn)
        parcel.writeString(deptNameEn)
        parcel.writeString(secNameEn)
        parcel.writeString(posNameEn)
        parcel.writeString(orgNameTh)
        parcel.writeString(deptNameTh)
        parcel.writeString(secNameTh)
        parcel.writeString(posNameTh)
        parcel.writeValue(jobGradeIdx)
        parcel.writeValue(jobGradeLevel)
        parcel.writeValue(costCenterIdx)
        parcel.writeString(costCenterNo)
        parcel.writeString(costCenterName)
        parcel.writeString(empStartDate)
        parcel.writeValue(empProbationStatus)
        parcel.writeString(empProbationDate)
        parcel.writeString(empCreatedate)
        parcel.writeString(empUpdatedate)
        parcel.writeValue(empStatus)
        parcel.writeString(nawPassword)
        parcel.writeValue(typeReset)
        parcel.writeValue(empIdxApprove1)
        parcel.writeValue(empIdxApprove2)
        parcel.writeString(empApprove1)
        parcel.writeString(empApprove2)
        parcel.writeString(ipAddress)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EmployeeDetail> {
        override fun createFromParcel(parcel: Parcel): EmployeeDetail {
            return EmployeeDetail(parcel)
        }

        override fun newArray(size: Int): Array<EmployeeDetail?> {
            return arrayOfNulls(size)
        }
    }

}

data class OrganizationDetail(
        @SerializedName("org_idx") var orgIdx: Int? = 0,
        @SerializedName("org_status") var orgStatus: Int? = 0,
        @SerializedName("org_name_en") var orgNameEn: String? = "",
        @SerializedName("org_name_th") var orgNameTh: String? = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(orgIdx)
        parcel.writeValue(orgStatus)
        parcel.writeString(orgNameEn)
        parcel.writeString(orgNameTh)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrganizationDetail> {
        override fun createFromParcel(parcel: Parcel): OrganizationDetail {
            return OrganizationDetail(parcel)
        }

        override fun newArray(size: Int): Array<OrganizationDetail?> {
            return arrayOfNulls(size)
        }
    }
}

data class DepartmentDetail(
        @SerializedName("rdept_idx") var rdeptIdx: Int? = 0,
        @SerializedName("rdept_status") var rdeptStatus: Int? = 0,

        @SerializedName("dept_idx") var deptIdx: Int? = 0,
        @SerializedName("dept_name_en") var deptNameEn: String? = "",
        @SerializedName("dept_name_th") var deptNameTh: String? = "",

        @SerializedName("org_idx") var orgIdx: Int? = 0,
        @SerializedName("org_name_en") var orgNameEn: String? = "",
        @SerializedName("org_name_th") var orgNameTh: String? = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(rdeptIdx)
        parcel.writeValue(rdeptStatus)
        parcel.writeValue(deptIdx)
        parcel.writeString(deptNameEn)
        parcel.writeString(deptNameTh)
        parcel.writeValue(orgIdx)
        parcel.writeString(orgNameEn)
        parcel.writeString(orgNameTh)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DepartmentDetail> {
        override fun createFromParcel(parcel: Parcel): DepartmentDetail {
            return DepartmentDetail(parcel)
        }

        override fun newArray(size: Int): Array<DepartmentDetail?> {
            return arrayOfNulls(size)
        }
    }
}

data class SectionDetail(
        @SerializedName("rsec_idx") var rsecIdx: Int? = 0,
        @SerializedName("rsec_status") var rsecStatus: Int? = 0,

        @SerializedName("sec_idx") var secIdx: Int? = 0,
        @SerializedName("sec_name_en") var secNameEn: String? = "",
        @SerializedName("sec_name_th") var secNameTh: String? = "",

        @SerializedName("rdept_idx") var rdeptIdx: Int? = 0,
        @SerializedName("dept_idx") var deptIdx: Int? = 0,
        @SerializedName("dept_name_en") var deptNameEn: String? = "",
        @SerializedName("dept_name_th") var deptNameTh: String? = "",

        @SerializedName("org_idx") var orgIdx: Int? = 0,
        @SerializedName("org_name_en") var orgNameEn: String? = "",
        @SerializedName("org_name_th") var orgNameTh: String? = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(rsecIdx)
        parcel.writeValue(rsecStatus)
        parcel.writeValue(secIdx)
        parcel.writeString(secNameEn)
        parcel.writeString(secNameTh)
        parcel.writeValue(rdeptIdx)
        parcel.writeValue(deptIdx)
        parcel.writeString(deptNameEn)
        parcel.writeString(deptNameTh)
        parcel.writeValue(orgIdx)
        parcel.writeString(orgNameEn)
        parcel.writeString(orgNameTh)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SectionDetail> {
        override fun createFromParcel(parcel: Parcel): SectionDetail {
            return SectionDetail(parcel)
        }

        override fun newArray(size: Int): Array<SectionDetail?> {
            return arrayOfNulls(size)
        }
    }
}

data class PositionDetail(
        @SerializedName("rpos_idx") var rposIdx: Int? = 0,
        @SerializedName("rpos_status") var rposStatus: Int? = 0,

        @SerializedName("pos_idx") var posIdx: Int? = 0,
        @SerializedName("pos_name_en") var posNameEn: String? = "",
        @SerializedName("pos_name_th") var posNameTh: String? = "",

        @SerializedName("rsec_idx") var rsecIdx: Int? = 0,
        @SerializedName("sec_idx") var secIdx: Int? = 0,
        @SerializedName("sec_name_en") var secNameEn: String? = "",
        @SerializedName("sec_name_th") var secNameTh: String? = "",

        @SerializedName("rdept_idx") var rdeptIdx: Int? = 0,
        @SerializedName("dept_idx") var deptIdx: Int? = 0,
        @SerializedName("dept_name_en") var deptNameEn: String? = "",
        @SerializedName("dept_name_th") var deptNameTh: String? = "",

        @SerializedName("org_idx") var orgIdx: Int? = 0,
        @SerializedName("org_name_en") var orgNameEn: String? = "",
        @SerializedName("org_name_th") var orgNameTh: String? = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(rposIdx)
        parcel.writeValue(rposStatus)
        parcel.writeValue(posIdx)
        parcel.writeString(posNameEn)
        parcel.writeString(posNameTh)
        parcel.writeValue(rsecIdx)
        parcel.writeValue(secIdx)
        parcel.writeString(secNameEn)
        parcel.writeString(secNameTh)
        parcel.writeValue(rdeptIdx)
        parcel.writeValue(deptIdx)
        parcel.writeString(deptNameEn)
        parcel.writeString(deptNameTh)
        parcel.writeValue(orgIdx)
        parcel.writeString(orgNameEn)
        parcel.writeString(orgNameTh)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PositionDetail> {
        override fun createFromParcel(parcel: Parcel): PositionDetail {
            return PositionDetail(parcel)
        }

        override fun newArray(size: Int): Array<PositionDetail?> {
            return arrayOfNulls(size)
        }
    }
}
