package tkn.dev.tknservices.view.state

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.View

/**
 * Created by lifetofree on 10/12/17.
 */
class BundleSavedState : View.BaseSavedState {
    private lateinit var bundle: Bundle

    fun getBundle(): Bundle {
        return bundle
    }

    fun setBundle(bundle: Bundle) {
        this.bundle = bundle
    }

    constructor(parcel: Parcel) : super(parcel) {
        // Read back
        bundle = parcel.readBundle()
    }

    constructor(superState: Parcelable) : super(superState) {}

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        super.writeToParcel(parcel, flags)
        // Write var here
        parcel.writeBundle(bundle)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BundleSavedState> {
        override fun createFromParcel(parcel: Parcel): BundleSavedState {
            return BundleSavedState(parcel)
        }

        override fun newArray(size: Int): Array<BundleSavedState?> {
            return arrayOfNulls(size)
        }
    }
}