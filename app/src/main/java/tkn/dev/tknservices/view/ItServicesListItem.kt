package tkn.dev.tknservices.view

import android.annotation.TargetApi
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.util.SparseArray
import android.widget.ImageView
import android.widget.TextView
import tkn.dev.tknservices.R
import tkn.dev.tknservices.view.state.BundleSavedState

/**
 * Created by lifetofree on 12/1/17.
 */
class ItServicesListItem : BaseCustomViewGroup {

    lateinit var tvDocCode: TextView
    lateinit var tvCreateDateUser: TextView
    lateinit var tvTimeCreateJob: TextView
    lateinit var tvEmpName: TextView
    lateinit var tvRDeptName: TextView
    lateinit var tvLocName: TextView
    lateinit var tvdetailUser: TextView
    lateinit var ImgStatus: ImageView
    lateinit var tvstatus: TextView

    constructor(context: Context) : super(context) {
        initInflate()
        initInstances()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, 0, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, defStyleAttr, 0)
    }

    @TargetApi(21)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, defStyleAttr, defStyleRes)
    }

    private fun initInflate() {
        inflate(getContext(), R.layout.list_it_services_item, this)
    }

    private fun initInstances() {
        // findViewById here
        tvDocCode = findViewById(R.id.tvDocCode)
        tvCreateDateUser = findViewById(R.id.tvCreateDateUser)
        tvTimeCreateJob = findViewById(R.id.tvTimeCreateJob)
        tvEmpName = findViewById(R.id.tvEmpName)
        tvRDeptName = findViewById(R.id.tvRDeptName)
        tvLocName = findViewById(R.id.tvLocName)
        tvdetailUser = findViewById(R.id.tvdetailUser)
        ImgStatus = findViewById(R.id.imgstatus)
        tvstatus = findViewById(R.id.tvstatus)
    }

    private fun initWithAttrs(attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) {

    }

    override fun onSaveInstanceState(): Parcelable {

        val superState: Parcelable = super.onSaveInstanceState()

        // Save Children's state as a Bundle
        val childrenStates = Bundle()
        for (i in 0 until childCount) {
            val id: Int = getChildAt(i).id
            if (id != 0) {
                val childrenState = SparseArray<Parcelable>()
                getChildAt(i).saveHierarchyState(childrenState)
                childrenStates.putSparseParcelableArray(id.toString(), childrenState)
            }
        }

        val bundle = Bundle()
        bundle.putBundle("childrenStates", childrenStates)

        // Save it to Parcelable
        val ss = BundleSavedState(superState)
        ss.setBundle(bundle)

        return ss
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
//        super.onRestoreInstanceState(state)
        val ss = state as BundleSavedState
        super.onRestoreInstanceState(ss.superState)

        // Restore SparseArray
        val childrenStates: Bundle = ss.getBundle().getBundle("childrenStates")

        // Restore Children's state
        for (i in 0 until childCount) {
            val id: Int = getChildAt(i).id
            if (id != 0) {
                if (childrenStates.containsKey(id.toString())) {
                    val childrenState = childrenStates.getSparseParcelableArray<Parcelable>(id.toString())
                    getChildAt(i).restoreHierarchyState(childrenState)
                }
            }
        }
    }


    fun setCaseText_doccode(text: String) {
        tvDocCode.text = text
    }

    fun setCaseText_CreateDateUser(text: String) {
        tvCreateDateUser.text = text
    }

    fun setCaseText_TimeCreateJob(text: String) {
        tvTimeCreateJob.text = " (" + text + ")"
    }

    fun setCaseText_EmpName(text: String) {
        tvEmpName.text = " : " + text
    }

    fun setCaseText_RDeptName(text: String) {
        tvRDeptName.text = " : " + text
    }

    fun setCaseText_LocName(text: String) {
        tvLocName.text = " : " + text
    }

    fun setCaseText_detailUser(text: String) {
        tvdetailUser.text = " : " + text
    }

    fun setCaseText_Status(status: String) {

        tvstatus.text = status

        when (status) {
            "เสร็จสมบูรณ์" -> ImgStatus.setImageResource(R.drawable.icon_status_completed)
            "ยังไม่รับงาน" -> ImgStatus.setImageResource(R.drawable.icon_status_warning)
            "กำลังตรวจสอบ", "กำลังดำเนินการ", "กำลังรอผู้แจ้งทำการตรวจสอบ" -> ImgStatus.setImageResource(R.drawable.icon_status_waiting)
            "ส่งเครม", "รออะไหล่",  "ติดต่อซัพภายนอก"-> ImgStatus.setImageResource(R.drawable.icon_status_repair)
        }
        
    }
}