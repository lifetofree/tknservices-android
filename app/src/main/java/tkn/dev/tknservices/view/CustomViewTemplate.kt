package tkn.dev.tknservices.view

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import tkn.dev.tknservices.view.state.BundleSavedState

/**
 * Created by lifetofree on 12/1/17.
 */
class CustomViewTemplate : View {
    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
        initWithAttrs(attrs, 0, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
        initWithAttrs(attrs, defStyleAttr, 0)
    }

    @TargetApi(21)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
        initWithAttrs(attrs, defStyleAttr, defStyleRes)
    }

    private fun init() {
        setSaveEnabled(true)
    }

    private fun initWithAttrs(attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) {
//        val a = getContext().getTheme().obtainStyledAttributes(
//                attrs,
//                R.styleable.StyleableName,
//                defStyleAttr, defStyleRes)
//        try {
//        } finally {
//            a.recycle()
//        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
    }

    override fun onSaveInstanceState(): Parcelable {
        val superState = super.onSaveInstanceState()
        val savedState = BundleSavedState(superState)
        // Save Instance State(s) here to the 'savedState.getBundle()'
        // for example,
        // savedState.getBundle().putString("key", value);
        return savedState
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        val ss = state as BundleSavedState
        super.onRestoreInstanceState(ss.getSuperState())
        val bundle = ss.getBundle()
        // Restore State from bundle here
    }
}