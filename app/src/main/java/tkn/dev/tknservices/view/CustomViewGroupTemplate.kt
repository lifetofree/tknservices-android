package tkn.dev.tknservices.view

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.util.SparseArray
import android.widget.FrameLayout
import tkn.dev.tknservices.R
import tkn.dev.tknservices.view.state.BundleSavedState

/**
 * Created by lifetofree on 12/1/17.
 */
class CustomViewGroupTemplate : BaseCustomViewGroup {
    constructor(context: Context) : super(context) {
        initInflate()
        initInstances()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, 0, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, defStyleAttr, 0)
    }

    @TargetApi(21)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initInflate()
        initInstances()
        initWithAttrs(attrs, defStyleAttr, defStyleRes)
    }

    private fun initInflate() {
        inflate(getContext(), R.layout.sample_layout, this)
    }

    private fun initInstances() {
        // findViewById here
    }

    private fun initWithAttrs(attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) {
//        val a = getContext().getTheme().obtainStyledAttributes(
//                attrs,
//                R.styleable.StyleableName,
//                defStyleAttr, defStyleRes)
//        try {
//        } finally {
//            a.recycle()
//        }
    }

    override fun onSaveInstanceState(): Parcelable {
//        return super.onSaveInstanceState()
        val superState: Parcelable = super.onSaveInstanceState()

        // Save Children's state as a Bundle
        val childrenStates = Bundle()
        for (i in 0 until childCount) {
            val id: Int = getChildAt(i).id
            if (id != 0) {
                val childrenState = SparseArray<Parcelable>()
                getChildAt(i).saveHierarchyState(childrenState)
                childrenStates.putSparseParcelableArray(id.toString(), childrenState)
            }
        }

        val bundle = Bundle()
        bundle.putBundle("childrenStates", childrenStates)

        // Save it to Parcelable
        val ss = BundleSavedState(superState)
        ss.setBundle(bundle)

        return ss
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
//        super.onRestoreInstanceState(state)
        val ss = state as BundleSavedState
        super.onRestoreInstanceState(ss.superState)

        // Restore SparseArray
        val childrenStates: Bundle = ss.getBundle().getBundle("childrenStates")

        // Restore Children's state
        for (i in 0 until childCount) {
            val id: Int = getChildAt(i).id
            if (id != 0) {
                if (childrenStates.containsKey(id.toString())) {
                    val childrenState = childrenStates.getSparseParcelableArray<Parcelable>(id.toString())
                    getChildAt(i).restoreHierarchyState(childrenState)
                }
            }
        }
    }
}