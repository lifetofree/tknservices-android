package tkn.dev.tknservices.manager;

import android.content.Context;

import tkn.dev.tknservices.dao.DataSupportIT;

/**
 * Created by lifetofree on 10/18/17.
 */

public class ItServicesListManager {

    private static ItServicesListManager instance;

    public static ItServicesListManager getInstance() {
        if (instance == null)
            instance = new ItServicesListManager();
        return instance;
    }

    private Context mContext;
    private DataSupportIT dao;

    private ItServicesListManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public DataSupportIT getDao() {
        return dao;
    }

    public void setDao(DataSupportIT dao) {
        this.dao = dao;
    }
}
