package tkn.dev.tknservices.manager;

import android.content.Context;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tkn.dev.tknservices.manager.http.ApiService;


/**
 * Created by lifetofree on 10/18/17.
 */

public class HttpManager {

    private static HttpManager instance;

    public static HttpManager getInstance() {
        if (instance == null)
            instance = new HttpManager();
        return instance;
    }

    private Context mContext;
    private ApiService service;

    private HttpManager() {
        mContext = Contextor.getInstance().getContext();

        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://172.16.11.5/services.taokaenoi.co.th/webapi/")
                .baseUrl("http://services.taokaenoi.co.th/webapi/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(ApiService.class);
    }

    public ApiService getService() {
        return service;
    }
}

