package tkn.dev.tknservices.manager.http

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


/**
 * Created by lifetofree on 11/9/17.
 */
interface ApiService {

    // Employee
    @FormUrlEncoded
    @POST("api_employee.asmx/CheckLogin")
    fun checkEmployee(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @GET("api_employee.asmx/GetMyProfile")
    fun getEmployee(@Query("emp_idx") empIdx: Int): Call<ResponseBody>
    // Employee

    // IT Services
    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_List")
    fun getItServicesList(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_List_mobile")
    fun getItServicesList_mobile(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_employee.asmx/GetPlace")
    fun getItServicesList_GetPlace(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/InsertItrepair")
    fun getItServicesList_Insert(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_Remote")
    fun getItServicesList_Select_Remote(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/SelectChooseOther")
    fun getItServicesList_SelectChooseOther(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/InsertItrepair")
    fun getItServicesList_InsertItrepair(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/InsertWorkTime")
    fun getItServicesList_InsertWorkTime(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_Holder")
    fun getItServicesList_Select_Holder(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_DetailCloseJobList")
    fun getItServicesList_Select_DetailCloseJobList(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_GvComment_List")
    fun getItServicesList_Select_GvComment_List(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_ddlLV1IT")
    fun getItServicesList_Select_ddlLV1IT(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_ddlLV2IT")
    fun getItServicesList_Select_ddlLV2IT(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_ddlLV3IT")
    fun getItServicesList_Select_ddlLV3IT(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_ddlLV4IT")
    fun getItServicesList_Select_ddlLV4IT(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Update_SapGetJob")
    fun getItServicesList_Update_SapGetJob(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Update_ITGetJob")
    fun getItServicesList_Update_ITGetJob(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Update_GMGetJob")
    fun getItServicesList_Update_GMGetJob(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Update_POSGetJob")
    fun getItServicesList_Update_POSGetJob(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Sap_CloseJob")
    fun getItServicesList_Sap_CloseJob(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/IT_CloseJob")
    fun getItServicesList_IT_CloseJob(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/GM_CloseJob")
    fun getItServicesList_GM_CloseJob(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/POS_CloseJob")
    fun getItServicesList_POS_CloseJob(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Update_ChangeSystem")
    fun getItServicesList_Update_ChangeSystem(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/InsertCommentIT")
    fun getItServicesList_InsertCommentIT(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/InsertCommentSAP")
    fun getItServicesList_InsertCommentSAP(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/InsertCommentGM")
    fun getItServicesList_InsertCommentGM(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/InsertCommentPOS")
    fun getItServicesList_InsertCommentPOS(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/InsertSAPList")
    fun getItServicesList_InsertSAPList(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/InsertGMList")
    fun getItServicesList_InsertGMList(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/InsertPOSList")
    fun getItServicesList_InsertPOSList(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_DetailList")
    fun getItServicesList_Select_DetailList(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/SelectCaseLV1POSU0")
    fun getItServicesList_SelectCaseLV1POSU0(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/SelectCaseLV2POSU0")
    fun getItServicesList_SelectCaseLV2POSU0(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/SelectCaseLV3POSU0")
    fun getItServicesList_SelectCaseLV3POSU0(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/SelectCaseLV4POSU0")
    fun getItServicesList_SelectCaseLV4POSU0(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_ddlLV1")
    fun getItServicesList_Select_ddlLV1(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_ddlLV2")
    fun getItServicesList_Select_ddlLV2(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_ddlLV3")
    fun getItServicesList_Select_ddlLV3(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_ddlLV4")
    fun getItServicesList_Select_ddlLV4(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_ddlLV5")
    fun getItServicesList_Select_ddlLV5(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_DocCode")
    fun getItServicesList_Select_DocCode(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api_itrepair.asmx/Select_Location")
    fun getItServicesList_Select_Location(@Field("jsonIn") jsonIn: String): Call<ResponseBody>

}