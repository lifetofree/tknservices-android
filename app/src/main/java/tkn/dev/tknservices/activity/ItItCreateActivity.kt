package tkn.dev.tknservices.activity

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import tkn.dev.tknservices.R
import android.widget.ArrayAdapter
//import com.sun.deploy.ui.CacheUpdateProgressDialog.dismiss
import android.content.DialogInterface
import android.app.AlertDialog
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import android.widget.EditText
import tkn.dev.tknservices.R.*
import tkn.dev.tknservices.fragment.*


class ItItCreateActivity : AppCompatActivity() {

    private val PREFS_NAME: String = "EmployeePrefs"
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var actionbarDrawerToggle: ActionBarDrawerToggle
    private lateinit var toolbar: Toolbar
    private var fragmentName: String = ""
    private lateinit var btnDrawerItHome: Button
    private lateinit var btnDrawerItLogout: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(layout.activity_it_it_create)


        initInstances()

        fragmentName = if (intent.getStringExtra("fragmentName") != null) {
            intent.getStringExtra("fragmentName")
        } else {
            ""
        }

        //button
        when (fragmentName) {
            "ItItReceive" ->
                supportFragmentManager.beginTransaction()
                        .add(R.id.contentContainer, ItItReceiveJobFragment.newInstance(), "ItItReceiveJobFragment")
                        .commit()
            "ItItJobTranfer" ->
                supportFragmentManager.beginTransaction()
                        .add(R.id.contentContainer, ItItJobTranferFragment.newInstance(), "ItItJobTranferFragment")
                        .commit()
            "ItItUserCloseJob" ->
                supportFragmentManager.beginTransaction()
                        .add(R.id.contentContainer, ItItUserCloseJobFragment.newInstance(), "ItItUserCloseJobFragment")
                        .commit()
            "ItItCloseJob" ->
                supportFragmentManager.beginTransaction()
                        .add(R.id.contentContainer, ItItCloseJobFragment.newInstance(), "ItItCloseJobFragment")
                        .commit()
            else ->
                supportFragmentManager.beginTransaction()
                        .add(R.id.contentContainer, ItItCreateFragment.newInstance(), "ItItCreateFragment")
                        .commit()
        }

    }


    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    private fun initInstances() {
        toolbar = findViewById(id.toolbar)
        toolbar.title = "IT"
        toolbar.setBackgroundResource(drawable.bg_title_it_it)

        setSupportActionBar(toolbar)

        drawerLayout = findViewById(id.drawerLayout)
        actionbarDrawerToggle = ActionBarDrawerToggle(
                this,
                drawerLayout,
                string.open_drawer,
                string.close_drawer
        )
        drawerLayout.addDrawerListener(actionbarDrawerToggle)

        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btnDrawerItHome = findViewById(R.id.btnDrawerItHome)
        btnDrawerItLogout = findViewById(R.id.btnDrawerItLogout)

        btnDrawerItHome.setOnClickListener(listener)
        btnDrawerItLogout.setOnClickListener(listener)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        actionbarDrawerToggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        actionbarDrawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (actionbarDrawerToggle.onOptionsItemSelected(item))
            return true
        return super.onOptionsItemSelected(item)
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when(view.id) {
            R.id.btnDrawerItHome -> {
                val intentHome = Intent(this, MainActivity::class.java)
                intentHome.putExtra("fragmentName", "centralized")
                startActivity(intentHome)
            }
            R.id.btnDrawerItLogout -> {
                // Clear value
                val settings: SharedPreferences = getSharedPreferences(PREFS_NAME, 0)
                val editor: SharedPreferences.Editor = settings.edit()
                editor.clear()
                editor.commit()

                val intentLogout = Intent(this, MainActivity::class.java)
                intentLogout.putExtra("fragmentName", "login")
                startActivity(intentLogout)
            }
        }
    }
}

