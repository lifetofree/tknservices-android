package tkn.dev.tknservices.activity

import android.content.SharedPreferences
import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import android.view.View
import android.widget.ListView
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.R
import tkn.dev.tknservices.fragment.CentralizedFragment
import tkn.dev.tknservices.fragment.LoginFragment
import java.io.IOException

class MainActivity : AppCompatActivity() {

    private val PREFS_NAME: String = "EmployeePrefs"
    private var fragmentName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_main)

        initInstances()

        // First Created
        // Place Fragment Here
        // Get/Check Fragment Name
        fragmentName = if (intent.getStringExtra("fragmentName") != null) {
            intent.getStringExtra("fragmentName")
        } else {
            ""
        }
        // Get SharedPreferences
        val settings: SharedPreferences = getSharedPreferences(PREFS_NAME, 0)
        when (fragmentName) {
            "centralized" -> {
                if (settings.getInt("empIdx", 0) > 0) {
                    supportFragmentManager.beginTransaction()
                            .add(R.id.contentContainer, CentralizedFragment.newInstance(), "CentralizedFragment")
                            .commit()
                } else {
                    supportFragmentManager.beginTransaction()
                            .add(R.id.contentContainer, LoginFragment.newInstance(), "LoginFragment")
                            .commit()
                }
            }
            "login" ->
                supportFragmentManager.beginTransaction()
                        .add(R.id.contentContainer, LoginFragment.newInstance(), "LoginFragment")
                        .commit()
            else -> {
                if (settings.getInt("empIdx", 0) > 0) {
                    supportFragmentManager.beginTransaction()
                            .add(R.id.contentContainer, CentralizedFragment.newInstance(), "CentralizedFragment")
                            .commit()
                } else {
                    supportFragmentManager.beginTransaction()
                            .add(R.id.contentContainer, LoginFragment.newInstance(), "LoginFragment")
                            .commit()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    private fun initInstances() {

    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
    }


}
