package tkn.dev.tknservices.activity

import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItPosListActivity
import tkn.dev.tknservices.fragment.ItPosListFragment

class ItPosListActivity : AppCompatActivity() {

    private val PREFS_NAME: String = "EmployeePrefs"
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var actionbarDrawerToggle: ActionBarDrawerToggle
    private lateinit var toolbar: Toolbar
    private lateinit var btnDrawerItHome: Button
    private lateinit var btnDrawerItLogout: Button
    private lateinit var btnToolbarCreate: ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_it_pos_list)

        initInstances()

        // First Created
        // Place Fragment Here
        supportFragmentManager.beginTransaction()
                .add(R.id.contentContainer, ItPosListFragment.newInstance(), "ItPosListFragment")
                .commit()
    }


    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    private fun initInstances() {
        toolbar = findViewById(R.id.toolbar)
        toolbar.title = "POS"
        toolbar.setBackgroundResource(R.drawable.bg_title_it_pos)

        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawerLayout)
        actionbarDrawerToggle = ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.string.open_drawer,
                R.string.close_drawer
        )
        drawerLayout.addDrawerListener(actionbarDrawerToggle)

        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btnDrawerItHome = findViewById(R.id.btnDrawerItHome)
        btnDrawerItLogout = findViewById(R.id.btnDrawerItLogout)
        btnToolbarCreate = findViewById(R.id.btnToolbarCreate)

        btnDrawerItHome.setOnClickListener(listener)
        btnDrawerItLogout.setOnClickListener(listener)
        btnToolbarCreate.setOnClickListener(listener)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        actionbarDrawerToggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        actionbarDrawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (actionbarDrawerToggle.onOptionsItemSelected(item))
            return true
        return super.onOptionsItemSelected(item)
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.btnDrawerItHome -> {
                val intentHome = Intent(this, MainActivity::class.java)
                intentHome.putExtra("fragmentName", "centralized")
                startActivity(intentHome)
            }
            R.id.btnDrawerItLogout -> {
                // Clear value
                val settings: SharedPreferences = getSharedPreferences(PREFS_NAME, 0)
                val editor: SharedPreferences.Editor = settings.edit()
                editor.clear()
                editor.commit()

                val intentLogout = Intent(this, MainActivity::class.java)
                intentLogout.putExtra("fragmentName", "login")
                startActivity(intentLogout)
            }
            R.id.btnToolbarCreate -> {
                val intentCreate = Intent(this, ItPosCreateActivity::class.java)
                startActivity(intentCreate)
            }
        }
    }
}
