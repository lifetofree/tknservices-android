package tkn.dev.tknservices

import android.app.Application

/**
 * Created by lifetofree on 10/16/17.
 */
class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        // Initialize thing(s) here
    }

    override fun onTerminate() {
        super.onTerminate()
    }
}