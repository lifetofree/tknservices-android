package tkn.dev.tknservices.fragment

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItGoogleListActivity
import tkn.dev.tknservices.activity.ItItListActivity

import android.support.v7.widget.AppCompatSpinner
import android.util.Log
import android.widget.*
import android.widget.AdapterView
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.dao.DataSupportIT
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.manager.HttpManager

class ItGoogleUserCloseJobFragment : Fragment() {

    var someVar: Int = 0
    private val PREFS_NAME: String = "EmployeePrefs"
    private lateinit var userRequestList: UserRequestList
    var EmpIDX: Int = 0
    var RsecIDX: Int = 0
    var OrgIDX: Int = 0
    var URQIDX: Int = 0
    var unidx: Int = 0
    var acidx: Int = 0

    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnItGoogleList: Button
    private lateinit var btnItGoogleCancel: Button
    private lateinit var App_status: AppCompatSpinner

    val status: MutableList<String> = mutableListOf()
    val readOnlyView_status: kotlin.collections.List<String> = status

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_google_user_close_job, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItGoogleUserCloseJobFragment {
            val fragment = ItGoogleUserCloseJobFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    fun SharedPreferences(){

        val settings: SharedPreferences = activity.getSharedPreferences(PREFS_NAME, 0)
        EmpIDX = settings.getInt("empIdx", 0).toString().toInt()
        RsecIDX = settings.getInt("rsecIdx", 0).toString().toInt()
        OrgIDX = settings.getInt("orgIdx", 0).toString().toInt()
    }

    private fun initInstances(rootView: View) {

        // TODO: Check intent value
        // Get data from intent
        userRequestList = activity.intent.getParcelableExtra("select")

        SharedPreferences()

        btnItGoogleList = rootView.findViewById(R.id.btnItGoogleList)
        btnItGoogleCancel = rootView.findViewById(R.id.btnItGoogleCancel)
        App_status = rootView.findViewById(R.id.sp_status_userclosejob)

        URQIDX = userRequestList.urqidx!!.toInt()
        unidx = userRequestList.unidx!!.toInt()
        acidx = userRequestList.acidx!!.toInt()

        btnItGoogleList.setOnClickListener(listener)
        btnItGoogleCancel.setOnClickListener(listener)

        spiner_status()
    }

    fun spiner_status() {

        status.add("สามารถใช้งานได้ตามปกติ")
        status.add("ยังพบอาการตามที่แจ้ง")
        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_status)
        App_status.adapter = adapter
        App_status.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                Toast.makeText(getActivity(), readOnlyView_status[position], Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }
    }

    fun update_user_closejob() {

        var temp_status: Int = 0
        var status: String = App_status.selectedItem.toString()

        if(unidx == 2 && acidx == 1){

            if(status == "สามารถใช้งานได้ตามปกติ"){
                temp_status = 11
            }
            else if(status == "ยังพบอาการตามที่แจ้ง"){
                temp_status = 12
            }
            else{
                temp_status = 0
            }
        }


        var dataSupportIT = DataSupportIT()
        var userRequestList = UserRequestList()
        userRequestList.urqidx = URQIDX
        userRequestList.adminidx = EmpIDX
        userRequestList.admindoingidx = EmpIDX
        userRequestList.pidx_add = 0

        userRequestList.unidx = unidx
        userRequestList.acidx = acidx
        userRequestList.staidx = temp_status

        dataSupportIT.BoxUserRequest = arrayListOf(userRequestList)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("DataSupportIT", "jsonIn = " + jsonIn)

        var call = HttpManager.getInstance().service.getItServicesList_Update_GMGetJob(jsonIn)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("DataSupportIT", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupport = jsonElement2.get("DataSupportIT").asJsonObject

                } else {
                    Log.d("DataSupportIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when(view.id) {
            R.id.btnItGoogleList -> {

                update_user_closejob()

                var itItList = Intent(activity, ItGoogleListActivity::class.java)
                startActivity(itItList)
            }
            R.id.btnItGoogleCancel -> {
                var itItList = Intent(activity, ItGoogleListActivity::class.java)
                startActivity(itItList)
            }
        }
    }
}