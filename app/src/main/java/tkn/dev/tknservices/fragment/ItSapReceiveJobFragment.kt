package tkn.dev.tknservices.fragment

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItSapListActivity

import android.support.v7.widget.AppCompatSpinner
import android.util.Log
import android.widget.*
import android.widget.AdapterView
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.dao.DataSupportIT
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.manager.HttpManager

class ItSapReceiveJobFragment : Fragment() {

    var someVar: Int = 0
    private val PREFS_NAME: String = "EmployeePrefs"
    private lateinit var userRequestList: UserRequestList
    var EmpIDX: Int = 0
    var RsecIDX: Int = 0
    var OrgIDX: Int = 0
    var URQIDX: Int = 0
    var unidx: Int = 0
    var acidx: Int = 0

    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnSapList: Button
    private lateinit var btnSapCancel: Button
    private lateinit var App_status: AppCompatSpinner

    val status: MutableList<String> = mutableListOf()
    val readOnlyView_status: kotlin.collections.List<String> = status

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_sap_receive, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItSapReceiveJobFragment {
            val fragment = ItSapReceiveJobFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    fun SharedPreferences(){

        val settings: SharedPreferences = activity.getSharedPreferences(PREFS_NAME, 0)
        EmpIDX = settings.getInt("empIdx", 0).toString().toInt()
        RsecIDX = settings.getInt("rsecIdx", 0).toString().toInt()
        OrgIDX = settings.getInt("orgIdx", 0).toString().toInt()
    }

    private fun initInstances(rootView: View) {

        // TODO: Check intent value
        // Get data from intent
        userRequestList = activity.intent.getParcelableExtra("select")

        SharedPreferences()

        btnSapList = rootView.findViewById(R.id.btnSapList)
        btnSapCancel = rootView.findViewById(R.id.btnSapCancel)
        App_status = rootView.findViewById(R.id.sp_status)

        URQIDX = userRequestList.urqidx!!.toInt()
        unidx = userRequestList.unidx!!.toInt()
        acidx = userRequestList.acidx!!.toInt()

        btnSapList.setOnClickListener(listener)
        btnSapCancel.setOnClickListener(listener)

        spiner_status()
    }

    fun spiner_status() {

        status.add("ด่วน")
        status.add("ไม่ด่วน")
        status.add("ปานกลาง")
        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_status)
        App_status.adapter = adapter
        App_status.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                Toast.makeText(getActivity(), readOnlyView_status[position], Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }
    }

    fun update_receive() {

        var PIDX_Add: Int = 0
        var staidx: Int = 0

        var statusreceive: String = App_status.selectedItem.toString()

        /**************************************************/
        if(unidx == 2 && acidx == 1){
            /*if(approve == 5){
                staidx = 24
            }
            else{*/
                staidx = 23
            //}
        }

        if(statusreceive == "ด่วน"){
            PIDX_Add = 1
        }
        else if(statusreceive == "ไม่ด่วน"){
            PIDX_Add = 2
        }
        else if(statusreceive == "ปานกลาง"){
            PIDX_Add = 3
        }

        var dataSupportIT = DataSupportIT()
        var userRequestList = UserRequestList()
        userRequestList.urqidx = URQIDX
        userRequestList.adminidx = EmpIDX
        userRequestList.admindoingidx = EmpIDX

        userRequestList.unidx = unidx
        userRequestList.acidx = acidx
        userRequestList.staidx = PIDX_Add


        dataSupportIT.BoxUserRequest = arrayListOf(userRequestList)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("DataSupportIT", "jsonIn = " + jsonIn)

        var call = HttpManager.getInstance().service.getItServicesList_Update_SapGetJob(jsonIn)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("DataSupportIT", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupport = jsonElement2.get("DataSupportIT").asJsonObject

                } else {
                    Log.d("DataSupportIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when(view.id) {
            R.id.btnSapList -> {

                update_receive()

                var itItList = Intent(activity, ItSapListActivity::class.java)
                startActivity(itItList)
            }
            R.id.btnSapCancel -> {
                var itItCancel = Intent(activity, ItSapListActivity::class.java)
                startActivity(itItCancel)
            }
        }
    }
}