package tkn.dev.tknservices.fragment

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItGoogleCreateActivity
import tkn.dev.tknservices.activity.ItGoogleListActivity
import tkn.dev.tknservices.activity.ItItListActivity
import tkn.dev.tknservices.activity.ItPosListActivity
import tkn.dev.tknservices.dao.DataSupportIT
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.manager.HttpManager

class ItGoogleCloseJobFragment : Fragment() {

    var someVar: Int = 0
    private val PREFS_NAME: String = "EmployeePrefs"
    private lateinit var userRequestList: UserRequestList
    var EmpIDX: Int = 0
    var RsecIDX: Int = 0
    var OrgIDX: Int = 0
    var URQIDX: Int = 0
    var unidx: Int = 0
    var acidx: Int = 0

    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnIt_Closejob_Save: Button
    private lateinit var btnIt_Closejob_Cancel: Button
    private lateinit var et_comment: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_google_closejob, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItGoogleCloseJobFragment {
            val fragment = ItGoogleCloseJobFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    fun SharedPreferences(){

        val settings: SharedPreferences = activity.getSharedPreferences(PREFS_NAME, 0)
        EmpIDX = settings.getInt("empIdx", 0).toString().toInt()
        RsecIDX = settings.getInt("rsecIdx", 0).toString().toInt()
    }

    private fun initInstances(rootView: View) {

        // TODO: Check intent value
        // Get data from intent
        userRequestList = activity.intent.getParcelableExtra("select")

        SharedPreferences()

        btnIt_Closejob_Save = rootView.findViewById(R.id.btnIt_Closejob_Save)
        btnIt_Closejob_Cancel = rootView.findViewById(R.id.btnIt_Closejob_Cancel)
        et_comment = rootView.findViewById(R.id.et_comment)

        URQIDX = userRequestList.urqidx!!.toInt()
        unidx = userRequestList.unidx!!.toInt()
        acidx = userRequestList.acidx!!.toInt()

        btnIt_Closejob_Save.setOnClickListener(listener)
        btnIt_Closejob_Cancel.setOnClickListener(listener)
    }


    fun urlUpdate() {

        var staidx: Int = 0

        if(unidx == 4 && acidx == 3){
            staidx = 22
        }


        var dataSupportIT = DataSupportIT()
        var userRequestList = UserRequestList()
        userRequestList.urqidx = URQIDX
        userRequestList.adminidx = EmpIDX

        userRequestList.unidx = unidx
        userRequestList.acidx = acidx
        userRequestList.staidx = staidx

        dataSupportIT.BoxUserRequest = arrayListOf(userRequestList)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("DataSupportIT", "jsonIn = " + jsonIn)

        var call = HttpManager.getInstance().service.getItServicesList_Update_GMGetJob(jsonIn)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("DataSupportIT", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupport = jsonElement2.get("DataSupportIT").asJsonObject

                } else {
                    Log.d("DataSupportIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    fun urlCloseJob_Admin() {

        var dataSupportIT = DataSupportIT()
        var userRequestList = UserRequestList()
        userRequestList.urqidx = URQIDX
        userRequestList.cit1idx = 0
        userRequestList.cit2idx = 0
        userRequestList.cit3idx = 0
        userRequestList.cit4idx = 0

        userRequestList.adminidx = EmpIDX
        userRequestList.admindoingidx = EmpIDX
        userRequestList.commentamdoing = et_comment.text.toString()
        userRequestList.ccaidx = 0
        userRequestList.recieveidx = 0
        userRequestList.empidx_add = EmpIDX

        userRequestList.unidx = unidx
        userRequestList.staidx = 1

        dataSupportIT.BoxUserRequest = arrayListOf(userRequestList)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("DataSupportIT", "jsonIn = " + jsonIn)

        var call = HttpManager.getInstance().service.getItServicesList_GM_CloseJob(jsonIn)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("DataSupportIT", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupport = jsonElement2.get("DataSupportIT").asJsonObject

                } else {
                    Log.d("DataSupportIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when(view.id) {
            R.id.btnIt_Closejob_Save -> {

                if(et_comment.text.toString() == ""){

                    val simpleAlert = AlertDialog.Builder(getActivity()).create()
                    simpleAlert.setTitle("แจ้งเตือน")
                    simpleAlert.setMessage("กรุณากรอกข้อมูลให้ครบถ้วน")

                    simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", { dialogInterface, i ->

                    })

                    simpleAlert.show()
                    Log.d("Insert", "No = ไม่สำเร็จ")

                }
                else{

                    urlUpdate()
                    urlCloseJob_Admin()

                    Log.d("Insert", "OK = สำเร็จ")

                    var itItList = Intent(activity, ItGoogleListActivity::class.java)
                    startActivity(itItList)
                }
            }
            R.id.btnIt_Closejob_Cancel -> {
                var itItList = Intent(activity, ItGoogleListActivity::class.java)
                startActivity(itItList)
            }
        }
    }
}