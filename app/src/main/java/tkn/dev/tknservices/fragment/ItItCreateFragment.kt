package tkn.dev.tknservices.fragment

import android.R.id.*
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItItListActivity
import android.widget.AdapterView.OnItemSelectedListener;
import java.util.ArrayList;
import java.util.List;

import android.support.v7.widget.AppCompatSpinner
import android.util.Log
import android.widget.*
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Toast
import android.support.v4.app.FragmentTransaction
import android.view.MotionEvent
import android.widget.Spinner
import android.widget.ArrayAdapter
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.dao.*
import tkn.dev.tknservices.manager.HttpManager


/**
 * Created by lifetofree on 10/19/17.
 */
class ItItCreateFragment : Fragment() {

    var someVar: Int = 0
    var emp_name: String = ""
    private val PREFS_NAME: String = "EmployeePrefs"
    var EmpIDX: Int = 0
    var RsecIDX: Int = 0
    var OrgIDX: Int = 0

    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnItCreate: Button
    private lateinit var btnItCancel: Button

    private lateinit var App_createuser: Spinner
    private lateinit var et_tel: EditText
    private lateinit var App_location: Spinner
    private lateinit var App_remote: Spinner
    private lateinit var App_holder: Spinner
    private lateinit var et_user: EditText
    private lateinit var et_pass: EditText
    private lateinit var et_comment: EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_it_it_create);

        // Initialize Spinners

    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_it_create, container, false)
        initInstances(rootView)

        /*val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        val values = arrayOf("Time at Residence", "Under 6 months", "6-12 months", "1-2 years", "2-4 years", "4-8 years", "8-15 years", "Over 15 years")
        val spinner = rootView.findViewById(R.id.spinner1) as Spinner
        val adapter = ArrayAdapter(this.activity, android.R.layout.simple_spinner_item, values)
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
        spinner.adapter = adapter*/


        return rootView
    }

    companion object {
        fun newInstance(): ItItCreateFragment {
            val fragment = ItItCreateFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    fun SharedPreferences(){

        val settings: SharedPreferences = activity.getSharedPreferences(PREFS_NAME, 0)
        EmpIDX = settings.getInt("empIdx", 0).toString().toInt()
        RsecIDX = settings.getInt("rsecIdx", 0).toString().toInt()
        OrgIDX = settings.getInt("orgIdx", 0).toString().toInt()
    }

    private fun initInstances(rootView: View) {

        SharedPreferences()

        btnItCreate = rootView.findViewById(R.id.btnItCreate)
        btnItCancel = rootView.findViewById(R.id.btnItCancel)
        App_createuser = rootView.findViewById(R.id.sp_createname)
        App_location = rootView.findViewById(R.id.sp_location)
        App_remote = rootView.findViewById(R.id.sp_remote)
        App_holder = rootView.findViewById(R.id.sp_holder)
        et_user = rootView.findViewById(R.id.et_user)
        et_pass = rootView.findViewById(R.id.et_pass)
        et_tel = rootView.findViewById(R.id.et_tel)
        et_comment = rootView.findViewById(R.id.et_comment)


        btnItCreate.setOnClickListener(listener)
        btnItCancel.setOnClickListener(listener)

        select_defult_user()
        select_user()
        select_remote()
        select_location()

    }

    fun select_defult_user(){

        val gson = GsonBuilder().create()

        var call = HttpManager.getInstance().service.getEmployee(EmpIDX) //คอยข้อมูลจาก Login
        call.enqueue(object: Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("Employee", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataEmployee = jsonElement2.get("data_employee").asJsonObject

                    val resultEmployee = dataEmployee.get("employee_list")
                    val employeeList = gson.fromJson(resultEmployee, EmployeeDetail::class.java)

                    val datauser_array = employeeList.empNameTh

                    Log.d("Employee", "empNameTH = " + datauser_array.toString())
                    emp_name = datauser_array.toString()

                } else {
                    Log.d("Employee", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }


    fun select_user() {

        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        boxUserRequest.rsecid = RsecIDX
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_SelectChooseOther(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val datauser_list = jsonElement2.get("DataSupportIT").asJsonObject

                    try {

                        val resultEmployee = datauser_list.get("BoxUserRequest")
                        val employeeList = gson.fromJson(resultEmployee, UserRequestList::class.java)

                        val user: MutableList<String> = mutableListOf()
                        user.add(0, "กรุณาเลือกข้อมูล")

                        val datauser_array = employeeList.emp_name_th

                        user.add(datauser_array.toString())
                        val readOnlyView_user: kotlin.collections.List<String> = user

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_user)
                        App_createuser.adapter = adapter
                        App_createuser.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_user[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }

                        var i: Int = 0
                        for (a in readOnlyView_user) {

                            if(a.equals(emp_name)){
                                App_createuser.setSelection(i)
                            }
                            i++
                        }
                    }

                    catch (e: Exception) {

                        try {

                            val dao_user = gson.fromJson(datauser_list, DataSupportIT::class.java)

                            val user: MutableList<String> = mutableListOf()
                            user.add(0, "กรุณาเลือกข้อมูล")

                            val datauser_array = dao_user.BoxUserRequest

                            if (datauser_array != null) {
                                for (a in datauser_array) {

                                    user.add(a.emp_name_th.toString())
                                }
                            }

                            val readOnlyView_User: kotlin.collections.List<String> = user

                            val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_User)
                            App_createuser.adapter = adapter
                            App_createuser.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                    Toast.makeText(getActivity(), readOnlyView_User[position], Toast.LENGTH_SHORT).show()

                                    var name: String = App_createuser.selectedItem.toString()

                                    if (name != "กรุณาเลือกข้อมูล") {
                                        select_holder(name)
                                    }
                                }

                                override fun onNothingSelected(adapterView: AdapterView<*>) {

                                }
                            }

                            var i: Int = 0
                            for (a in readOnlyView_User) {

                                if(a.equals(emp_name)){
                                    App_createuser.setSelection(i)
                                }
                                i++
                            }


                        }
                        catch (e: Exception) {

                            val user: MutableList<String> = mutableListOf()
                            user.add(0, "กรุณาเลือกข้อมูล")
                            val readOnlyView_user: kotlin.collections.List<String> = user

                            val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_user)
                            App_createuser.adapter = adapter
                            App_createuser.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                    Toast.makeText(getActivity(), readOnlyView_user[position], Toast.LENGTH_SHORT).show()
                                }

                                override fun onNothingSelected(adapterView: AdapterView<*>) {

                                }
                            }

                        }
                    }

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }

    fun select_remote() {

        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        boxUserRequest.remoteidx = 0
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_Select_Remote(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataremote_list = jsonElement2.get("DataSupportIT").asJsonObject

                    val dao_remote = gson.fromJson(dataremote_list, DataSupportIT::class.java)

                    val remote: MutableList<String> = mutableListOf()
                    remote.add(0, "กรุณาเลือกข้อมูล")

                    val dataRemote_array = dao_remote.BoxUserRequest

                    Log.d("Anser_remote", "item list = " + dataRemote_array.toString())

                    if (dataRemote_array != null) {
                        for (a in dataRemote_array) {

                            remote.add(a.remotename.toString())
                        }
                    }

                    val readOnlyView_remote: kotlin.collections.List<String> = remote

                    val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_remote)
                    App_remote.adapter = adapter
                    App_remote.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                            Toast.makeText(getActivity(), readOnlyView_remote[position], Toast.LENGTH_SHORT).show()
                        }

                        override fun onNothingSelected(adapterView: AdapterView<*>) {

                        }
                    }

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    fun select_holder(name: String) {

        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        boxUserRequest.deviceetc = name
        boxUserRequest.staidx = 1 //defult mode mobile
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        var call = HttpManager.getInstance().service.getItServicesList_Select_Holder(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val datauser_list = jsonElement2.get("DataSupportIT").asJsonObject

                    try {

                        val resultEmployee = datauser_list.get("BoxUserRequest")
                        val employeeList = gson.fromJson(resultEmployee, UserRequestList::class.java)

                        Log.d("Anser_Holder", "item list = " + resultEmployee.toString())
                        val user: MutableList<String> = mutableListOf()
                        user.add(0, "กรุณาเลือกข้อมูล")

                        val datauser_array = employeeList.u0_code

                        user.add(datauser_array.toString())
                        val readOnlyView_user: kotlin.collections.List<String> = user

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_user)
                        App_holder.adapter = adapter
                        App_holder.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_user[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }

                        App_holder.setSelection(1)
                    }

                    catch (e: Exception) {

                        try {

                            val employeeList = gson.fromJson(datauser_list, DataSupportIT::class.java)

                            val user: MutableList<String> = mutableListOf()
                            user.add(0, "กรุณาเลือกข้อมูล")

                            val datauser_array = employeeList.BoxUserRequest

                            if (datauser_array != null) {
                                for (a in datauser_array) {

                                    user.add(a.u0_code.toString())
                                }
                            }

                            val readOnlyView_user: kotlin.collections.List<String> = user

                            val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_user)
                            App_holder.adapter = adapter
                            App_holder.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                    Toast.makeText(getActivity(), readOnlyView_user[position], Toast.LENGTH_SHORT).show()
                                }

                                override fun onNothingSelected(adapterView: AdapterView<*>) {

                                }
                            }

                            App_holder.setSelection(1)
                        }
                        catch (e: Exception) {

                            val user: MutableList<String> = mutableListOf()
                            user.add(0, "กรุณาเลือกข้อมูล")

                            val readOnlyView_user: kotlin.collections.List<String> = user

                            val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_user)
                            App_holder.adapter = adapter
                            App_holder.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                    Toast.makeText(getActivity(), readOnlyView_user[position], Toast.LENGTH_SHORT).show()
                                }

                                override fun onNothingSelected(adapterView: AdapterView<*>) {

                                }
                            }
                        }

                    }

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }

    fun select_location() {


        var dataEmployee = DataSupportIT()
        var _dataemployee = data_employeelist()
        _dataemployee.org_idx = 1
        dataEmployee.employee_list = arrayListOf(_dataemployee)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataEmployee)
        val jsonObject = JsonObject()
        //jsonObject.add("data_employee", jsonElement)
        jsonObject.add("data_employee", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("Go", "return code = " + jsonElement)

        var call = HttpManager.getInstance().service.getItServicesList_GetPlace(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataemployee_list = jsonElement2.get("data_employee").asJsonObject

                    val dao_employee = gson.fromJson(dataemployee_list, DataSupportIT::class.java)

                    //Log.d("Ser", "item list = " + returnVal.toString())

                    val location: MutableList<String> = mutableListOf()
                    location.add(0, "กรุณาเลือกข้อมูล")

                    val dataEmployee_array = dao_employee.employee_list

                    Log.d("Anser", "item list = " + dataEmployee_array.toString())

                    if (dataEmployee_array != null) {
                        for (i in dataEmployee_array) {

                            location.add(i.LocName.toString())
                        }
                    }



                    val readOnlyView_location: kotlin.collections.List<String> = location //listOf(dataSupportIT)


                    val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_location)
                    App_location.adapter = adapter
                    App_location.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                            Toast.makeText(getActivity(), readOnlyView_location[position], Toast.LENGTH_SHORT).show()
                        }

                        override fun onNothingSelected(adapterView: AdapterView<*>) {

                        }
                    }

                    /*val returnCode = dataSupportIT.get("ReturnCode").asInt
                    Log.d("ServicesIT", "return code = " + returnCode.toString())

                    if (returnCode == 0) {
                        val dao = gson.fromJson(dataSupportIT, DataSupportIT::class.java)
                        Log.d("ServicesIT", "dao = " + dao.toString())
                        ItServicesListManager.getInstance().dao = dao
                        //listAdapter.notifyDataSetChanged()
                    } else {
                        // TODO : toast error
                    }*/
                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    fun Insert_it() {

        var remote_idx: Int = 0
        var CheckRemote: Int = 0

        var createuser: String = App_createuser.selectedItem.toString()
        var tel: String = et_tel.text.toString()
        var location: String = App_location.selectedItem.toString()
        var remote: String = App_remote.selectedItem.toString()
        var holder: String = App_holder.selectedItem.toString()
        var comment: String = et_comment.text.toString()
        var user_remote: String = et_user.text.toString()
        var pass_remote: String = et_pass.text.toString()

        if(remote == "Teamviewer"){
            remote_idx = 2
            CheckRemote = 1
        }
        else if(remote == "RemoteDesktop"){
            remote_idx = 39
            CheckRemote = 1
        }
        else{
            remote_idx = 2
            CheckRemote = 0
        }

        var dataSupportIT = DataSupportIT()
        var userRequestList = UserRequestList()
        userRequestList.fileuser = 0
        userRequestList.empidx_add = EmpIDX
        userRequestList.locname = location
        userRequestList.sysidx_add = 3
        userRequestList.iso_user = "3"
        userRequestList.teletc = tel
        userRequestList.emailetc = ""
        userRequestList.costidx = 0
        userRequestList.ncempidx = EmpIDX
        userRequestList.checkremote = CheckRemote
        userRequestList.remoteidx = remote_idx
        userRequestList.useridremote = user_remote
        userRequestList.passwordremote = pass_remote

        userRequestList.adminidx = 0
        userRequestList.admindoingidx = 0
        userRequestList.commentamdoing = ""
        userRequestList.fileamdoing = 0
        userRequestList.ccaidx = 0
        userRequestList.staidx = 1
        userRequestList.emailidx = 1 // Defult User Stored Mobile
        userRequestList.detailuser = comment
        userRequestList.deviceetc = holder
        userRequestList.emp_name_th = createuser

        dataSupportIT.BoxUserRequest = arrayListOf(userRequestList)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("DataSupportIT", "jsonIn = " + jsonIn)

        var call = HttpManager.getInstance().service.getItServicesList_Insert(jsonIn)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("DataSupportIT", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupport = jsonElement2.get("DataSupportIT").asJsonObject

                } else {
                    Log.d("DataSupportIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.btnItCreate -> {

                if( App_createuser.selectedItem.toString() == "กรุณาเลือกข้อมูล" || App_location.selectedItem.toString() == "กรุณาเลือกข้อมูล" ||
                        App_holder.selectedItem.toString() == "กรุณาเลือกข้อมูล" || App_holder.selectedItem.toString() == "" ||
                        App_remote.selectedItem.toString() == "กรุณาเลือกข้อมูล" || et_comment.text.toString() == ""){

                    val simpleAlert = AlertDialog.Builder(getActivity()).create()
                    simpleAlert.setTitle("แจ้งเตือน")
                    simpleAlert.setMessage("กรุณากรอกข้อมูลให้ครบถ้วน")

                    simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", { dialogInterface, i ->

                    })

                    simpleAlert.show()
                    Log.d("Insert", "No = ไม่สำเร็จ")

                }
                else{
                    Insert_it()
                    Log.d("Insert", "OK = สำเร็จ")

                    var itItList = Intent(activity, ItItListActivity::class.java)
                    startActivity(itItList)
                }
            }

            R.id.btnItCancel -> {
                var itItCancel = Intent(activity, ItItListActivity::class.java)
                startActivity(itItCancel)
            }
        }
    }
}