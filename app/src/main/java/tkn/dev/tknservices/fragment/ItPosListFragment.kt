package tkn.dev.tknservices.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.*
import tkn.dev.tknservices.adapter.ItServicesListAdapter
import tkn.dev.tknservices.dao.DataSupportIT
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.manager.HttpManager
import tkn.dev.tknservices.manager.ItServicesListManager

/**
 * Created by lifetofree on 10/19/17.
 */
class ItPosListFragment : Fragment() {

    var someVar: Int = 0
    lateinit var listView: ListView
    lateinit var listAdapter: ItServicesListAdapter

    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnItItList: Button
    private lateinit var btnSapDetailList: Button
    private lateinit var btnItAdminllList: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_pos_list, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItPosListFragment {
            val fragment = ItPosListFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    private fun initInstances(rootView: View) {

        //btnItItList = rootView.findViewById(R.id.btnItItList)
        //btnSapDetailList = rootView.findViewById(R.id.btnSapDetailList)
        //btnItAdminllList = rootView.findViewById(R.id.btnItAdminllList)

        //btnItItList.setOnClickListener(listener)
        //btnSapDetailList.setOnClickListener(listener)
        //btnItAdminllList.setOnClickListener(listener)

        listView = rootView.findViewById(R.id.listView_pos)
        listAdapter = ItServicesListAdapter()
        listView.adapter = listAdapter

        select_listview()
    }

    fun select_listview() {


        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        boxUserRequest.sysidx_add = 20
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        var call = HttpManager.getInstance().service.getItServicesList_mobile(jsonIn)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupportIT = jsonElement2.get("DataSupportIT").asJsonObject

                    val returnCode = dataSupportIT.get("ReturnCode").asInt
                    Log.d("Services", "return code = " + returnCode.toString())

                    if (returnCode == 0) {
                        // TODO: Check result is object or array
                        val dao = gson.fromJson(dataSupportIT, DataSupportIT::class.java)
                        Log.d("ServicesIT", "dao = " + dao.toString())
                        ItServicesListManager.getInstance().dao = dao
                        listAdapter.notifyDataSetChanged()

                        // Set onItemClickListener
                        listView.onItemClickListener = AdapterView.OnItemClickListener({ adapterView: AdapterView<*>?, view: View?, i: Int, l: Long ->
//                            Toast.makeText(activity,
//                                    "on click = " + dao.BoxUserRequest!![i].urqidx.toString(),
//                                    Toast.LENGTH_SHORT)
//                                    .show()
                            var selectedData: UserRequestList = dao.BoxUserRequest!![i]
                            val intent = Intent(activity, ItPosDetailActivity::class.java)
                            intent.putExtra("selectedData", selectedData)
                            startActivity(intent)
                        })
                    } else {
                        // TODO : toast error
                    }
                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("ServicesIT", "failure = " + t.toString())
            }
        })
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when(view.id) {

            /*R.id.btnItItList -> {
                var itItList = Intent(activity, ItPosCreateActivity::class.java)
                startActivity(itItList)
            }
            R.id.btnSapDetailList -> {
                var ItDetail = Intent(activity, ItPosDetailActivity::class.java)
                startActivity(ItDetail)
            }
            R.id.btnItAdminllList -> {
                val twoFragment = ItPosAdminFragment()
                val transaction = fragmentManager.beginTransaction()
                transaction.replace(R.id.contentContainer, twoFragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }*/
        }
    }
}