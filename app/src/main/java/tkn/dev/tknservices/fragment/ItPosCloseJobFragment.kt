package tkn.dev.tknservices.fragment

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItPosListActivity
import tkn.dev.tknservices.dao.DataSupportIT
import tkn.dev.tknservices.dao.POSList
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.manager.HttpManager

class ItPosCloseJobFragment : Fragment() {

    var someVar: Int = 0
    private val PREFS_NAME: String = "EmployeePrefs"
    private lateinit var userRequestList: UserRequestList
    var EmpIDX: Int = 0
    var RsecIDX: Int = 0
    var OrgIDX: Int = 0
    var URQIDX: Int = 0
    var unidx: Int = 0
    var acidx: Int = 0

    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnIt_Closejob_Save: Button
    private lateinit var btnIt_Closejob_Cancel: Button

    private lateinit var et_comment: EditText
    private lateinit var App_pos_lv1: Spinner
    private lateinit var App_pos_lv2: Spinner
    private lateinit var App_pos_lv3: Spinner
    private lateinit var App_pos_lv4: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_pos_closejob, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItPosCloseJobFragment {
            val fragment = ItPosCloseJobFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    fun SharedPreferences(){

        val settings: SharedPreferences = activity.getSharedPreferences(PREFS_NAME, 0)
        EmpIDX = settings.getInt("empIdx", 0).toString().toInt()
        RsecIDX = settings.getInt("rsecIdx", 0).toString().toInt()
        OrgIDX = settings.getInt("orgIdx", 0).toString().toInt()
    }

    private fun initInstances(rootView: View) {

        // TODO: Check intent value
        // Get data from intent
        userRequestList = activity.intent.getParcelableExtra("select")

        SharedPreferences()

        btnIt_Closejob_Save = rootView.findViewById(R.id.btnIt_Closejob_Save)
        btnIt_Closejob_Cancel = rootView.findViewById(R.id.btnIt_Closejob_Cancel)

        et_comment = rootView.findViewById(R.id.et_comment)
        App_pos_lv1 = rootView.findViewById(R.id.sp_pos_lv1)
        App_pos_lv2 = rootView.findViewById(R.id.sp_pos_lv2)
        App_pos_lv3 = rootView.findViewById(R.id.sp_pos_lv3)
        App_pos_lv4 = rootView.findViewById(R.id.sp_pos_lv4)

        URQIDX = userRequestList.urqidx!!.toInt()
        unidx = userRequestList.unidx!!.toInt()
        acidx = userRequestList.acidx!!.toInt()

        btnIt_Closejob_Save.setOnClickListener(listener)
        btnIt_Closejob_Cancel.setOnClickListener(listener)

        select_pos_lv1()

    }

    fun select_pos_lv1() {

        var dataSupportIT = DataSupportIT()
        var boxPos = POSList()
        //boxPos.pos1idx = 0
        dataSupportIT.BoxPOSList = arrayListOf(boxPos)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_SelectCaseLV1POSU0(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val datalv_list = jsonElement2.get("DataSupportIT").asJsonObject

                    //try {

                        val dao_lv = gson.fromJson(datalv_list, DataSupportIT::class.java)

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")

                        val datalv_array = dao_lv.BoxPOSList

                        Log.d("Anser_user", "item list = " + datalv_array.toString())

                        if (datalv_array != null) {
                            for (a in datalv_array) {

                                lv.add(a.name_code1.toString())
                            }
                        }

                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_pos_lv1.adapter = adapter
                        App_pos_lv1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()

                                var name: String = App_pos_lv1.selectedItem.toString()
                                Log.d("App_createuser", "item list = " + name)
                                if (name != "กรุณาเลือกข้อมูล") {
                                    select_pos_lv2(name)
                                }
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    /*}

                    catch (e: Exception) {

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")
                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_pos_lv1.adapter = adapter
                        App_pos_lv1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }

                    }*/

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    fun select_pos_lv2(name: String) {

        Log.d("App_c", "item = " + name)
        var dataSupportIT = DataSupportIT()
        var boxPos = POSList()
        boxPos.pos1_name = name
        //boxPos.pos1idx = 0
        dataSupportIT.BoxPOSList = arrayListOf(boxPos)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_SelectCaseLV2POSU0(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()
                    var newString = returnVal!!.replace(",\"ReturnCode\":[\"0\",\"0\"]", "")
                    val jsonElement_c = gson.fromJson(newString, JsonObject::class.java)
                    val datalv_list = jsonElement_c.get("DataSupportIT").asJsonObject

                    Log.d("App_createuser", "item list = " + jsonElement_c.toString())

                    //try {

                        val dao_lv = gson.fromJson(datalv_list, DataSupportIT::class.java)
                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")
                        val datalv_array = dao_lv.BoxPOSList

                        //Log.d("Anser_user", "item list = " + datalv_array.toString())

                        if (datalv_array != null) {
                            for (a in datalv_array) {

                                lv.add(a.name_code2.toString())
                            }
                        }

                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_pos_lv2.adapter = adapter
                        App_pos_lv2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()

                                var name: String = App_pos_lv1.selectedItem.toString()
                                Log.d("App_createuser", "item list = " + name)
                                if(name != "กรุณาเลือกข้อมูล"){
                                    select_pos_lv3(name)
                                }
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }

                    /*}
                    catch (e: Exception) {

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")
                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_pos_lv2.adapter = adapter
                        App_pos_lv2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }*/

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }

    fun select_pos_lv3(name: String) {

        var dataSupportIT = DataSupportIT()
        var boxPos = POSList()
        boxPos.pos2_name = name
        //boxPos.pos1idx = 0 //web
        dataSupportIT.BoxPOSList = arrayListOf(boxPos)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_SelectCaseLV3POSU0(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val datalv_list = jsonElement2.get("DataSupportIT").asJsonObject

                    try {

                        val dao_lv = gson.fromJson(datalv_list, DataSupportIT::class.java)

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")

                        val datalv_array = dao_lv.BoxPOSList

                        Log.d("Anser_user", "item list = " + datalv_array.toString())

                        if (datalv_array != null) {
                            for (a in datalv_array) {

                                lv.add(a.name_code3.toString())
                            }
                        }

                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_pos_lv3.adapter = adapter
                        App_pos_lv3.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()

                                var name: String = App_pos_lv1.selectedItem.toString()
                                Log.d("App_createuser", "item list = " + name)
                                if (name != "กรุณาเลือกข้อมูล") {
                                    select_pos_lv4(name)
                                }
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }

                    catch (e: Exception) {

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")
                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_pos_lv3.adapter = adapter
                        App_pos_lv3.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }

    fun select_pos_lv4(name: String) {

        var dataSupportIT = DataSupportIT()
        var boxPos = POSList()
        boxPos.pos3_name = name
        //boxPos.pos1idx = 0 //web
        dataSupportIT.BoxPOSList = arrayListOf(boxPos)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_SelectCaseLV4POSU0(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val datalv_list = jsonElement2.get("DataSupportIT").asJsonObject

                    try {

                        val dao_lv = gson.fromJson(datalv_list, DataSupportIT::class.java)

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")

                        val datalv_array = dao_lv.BoxPOSList

                        Log.d("Anser_user", "item list = " + datalv_array.toString())

                        if (datalv_array != null) {
                            for (a in datalv_array) {

                                lv.add(a.name_code4.toString())
                            }
                        }

                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_pos_lv4.adapter = adapter
                        App_pos_lv4.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }

                    catch (e: Exception) {

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")
                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_pos_lv4.adapter = adapter
                        App_pos_lv4.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }

                    }


                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }

    fun urlUpdate() {

        var staidx: Int = 0

        if(unidx == 2 && acidx == 1){
            staidx = 30
        }
        else{
            staidx = 29
        }

        var dataSupportIT = DataSupportIT()
        var userRequestList = UserRequestList()
        userRequestList.urqidx = URQIDX
        userRequestList.adminidx = EmpIDX

        userRequestList.unidx = unidx
        userRequestList.acidx = acidx
        userRequestList.staidx = staidx

        dataSupportIT.BoxUserRequest = arrayListOf(userRequestList)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("DataSupportIT", "jsonIn = " + jsonIn)

        var call = HttpManager.getInstance().service.getItServicesList_Update_POSGetJob(jsonIn)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("DataSupportIT", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupport = jsonElement2.get("DataSupportIT").asJsonObject

                } else {
                    Log.d("DataSupportIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    fun urlCloseJob_Admin() {

        var dataSupportIT = DataSupportIT()
        var userRequestList = UserRequestList()
        userRequestList.urqidx = URQIDX
        userRequestList.cit1idx = 0
        userRequestList.cit2idx = 0
        userRequestList.cit3idx = 0
        userRequestList.cit4idx = 0

        userRequestList.name_code1 = App_pos_lv1.selectedItem.toString()
        userRequestList.name_code2 = App_pos_lv2.selectedItem.toString()
        userRequestList.name_code3 = App_pos_lv3.selectedItem.toString()
        userRequestList.name_code4 = App_pos_lv4.selectedItem.toString()

        userRequestList.adminidx = EmpIDX
        userRequestList.admindoingidx = EmpIDX
        userRequestList.commentamdoing = et_comment.text.toString()
        userRequestList.ccaidx = 0
        userRequestList.recieveidx = 0
        userRequestList.empidx_add = EmpIDX
        userRequestList.unidx = unidx
        userRequestList.staidx = 1

        dataSupportIT.BoxUserRequest = arrayListOf(userRequestList)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("DataSupportIT", "jsonIn = " + jsonIn)

        var call = HttpManager.getInstance().service.getItServicesList_POS_CloseJob(jsonIn)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("DataSupportIT", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupport = jsonElement2.get("DataSupportIT").asJsonObject

                } else {
                    Log.d("DataSupportIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when(view.id) {
            R.id.btnIt_Closejob_Save -> {

                if(et_comment.text.toString() == "" || App_pos_lv1.selectedItem.toString() == "กรุณาเลือกข้อมูล" ||
                        App_pos_lv2.selectedItem.toString() == "กรุณาเลือกข้อมูล" || App_pos_lv3.selectedItem.toString() == "กรุณาเลือกข้อมูล" ||
                        App_pos_lv4.selectedItem.toString() == "กรุณาเลือกข้อมูล"){

                    val simpleAlert = AlertDialog.Builder(getActivity()).create()
                    simpleAlert.setTitle("แจ้งเตือน")
                    simpleAlert.setMessage("กรุณากรอกข้อมูลให้ครบถ้วน")

                    simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", { dialogInterface, i ->

                    })

                    simpleAlert.show()
                    Log.d("Insert", "No = ไม่สำเร็จ")

                }
                else{

                    urlUpdate()
                    urlCloseJob_Admin()

                    Log.d("Insert", "OK = สำเร็จ")

                    var itItList = Intent(activity, ItPosListActivity::class.java)
                    startActivity(itItList)
                }

            }
            R.id.btnIt_Closejob_Cancel -> {
                var itItList = Intent(activity, ItPosListActivity::class.java)
                startActivity(itItList)
            }
        }
    }
}