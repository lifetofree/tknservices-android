package tkn.dev.tknservices.fragment

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItGoogleListActivity

import android.support.v7.widget.AppCompatSpinner
import android.util.Log
import android.widget.*
import android.widget.AdapterView
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.dao.DataSupportIT
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.manager.HttpManager

class ItGoogleJobTranferFragment : Fragment() {

    var someVar: Int = 0
    private val PREFS_NAME: String = "EmployeePrefs"
    private lateinit var userRequestList: UserRequestList
    var EmpIDX: Int = 0
    var RsecIDX: Int = 0
    var OrgIDX: Int = 0
    var URQIDX: Int = 0
    var unidx: Int = 0
    var acidx: Int = 0


    private lateinit var tvCustomTitleBar: TextView
    private lateinit var BtnItGoogleList: Button
    private lateinit var btnItGoogleCancel: Button
    private lateinit var App_system: Spinner

    val system: MutableList<String> = mutableListOf()
    val readOnlyView_system: kotlin.collections.List<String> = system

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_google_jobtranfer, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItGoogleJobTranferFragment {
            val fragment = ItGoogleJobTranferFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    fun SharedPreferences(){

        val settings: SharedPreferences = activity.getSharedPreferences(PREFS_NAME, 0)
        EmpIDX = settings.getInt("empIdx", 0).toString().toInt()
        RsecIDX = settings.getInt("rsecIdx", 0).toString().toInt()
        OrgIDX = settings.getInt("orgIdx", 0).toString().toInt()
    }

    private fun initInstances(rootView: View) {

        // TODO: Check intent value
        // Get data from intent
        userRequestList = activity.intent.getParcelableExtra("select")
        Log.d("Data", "success body = " + userRequestList.toString())

        SharedPreferences()

        BtnItGoogleList = rootView.findViewById(R.id.btnItgoogleList)
        btnItGoogleCancel = rootView.findViewById(R.id.btnItGoogleCancel)
        App_system = rootView.findViewById(R.id.sp_system)

        URQIDX = userRequestList.urqidx!!.toInt()
        unidx = userRequestList.unidx!!.toInt()
        acidx = userRequestList.acidx!!.toInt()

        BtnItGoogleList.setOnClickListener(listener)
        btnItGoogleCancel.setOnClickListener(listener)

        spiner_system()
    }

    fun spiner_system() {

        system.add("SAP")
        system.add("IT")
        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_system)
        App_system.adapter = adapter
        App_system.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                Toast.makeText(getActivity(), readOnlyView_system[position], Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }
    }

    fun change_system() {

        var temp_system: Int = 0
        var system: String = App_system.selectedItem.toString()

        if(system == "Google Apps"){
            temp_system = 1
        }
        else if(system == "SAP"){
            temp_system = 2
        }
        else if(system == "IT"){
            temp_system = 3
        }

        var dataSupportIT = DataSupportIT()
        var userRequestList = UserRequestList()
        userRequestList.urqidx = URQIDX
        userRequestList.adminidx = EmpIDX
        userRequestList.admindoingidx = EmpIDX
        userRequestList.staidx = 0
        userRequestList.sysidx_add = temp_system
        userRequestList.orgidx = OrgIDX
        userRequestList.commentamdoing = "โอนย้ายไประบบ" + system
        userRequestList.acidx = acidx

        dataSupportIT.BoxUserRequest = arrayListOf(userRequestList)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("DataSupportIT", "jsonIn = " + jsonIn)

        var call = HttpManager.getInstance().service.getItServicesList_Update_ChangeSystem(jsonIn)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("DataSupportIT", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupport = jsonElement2.get("DataSupportIT").asJsonObject

                } else {
                    Log.d("DataSupportIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when(view.id) {
            R.id.btnItgoogleList -> {

                change_system()

                var itItList = Intent(activity, ItGoogleListActivity::class.java)
                startActivity(itItList)
            }
            R.id.btnItGoogleCancel -> {
                var itItCancel = Intent(activity, ItGoogleListActivity::class.java)
                startActivity(itItCancel)
            }
        }
    }
}