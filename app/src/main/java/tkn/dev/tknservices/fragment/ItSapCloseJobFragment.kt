package tkn.dev.tknservices.fragment

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItSapListActivity
import tkn.dev.tknservices.dao.DataSupportIT
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.manager.HttpManager

class ItSapCloseJobFragment : Fragment() {

    var someVar: Int = 0
    private val PREFS_NAME: String = "EmployeePrefs"
    private lateinit var userRequestList: UserRequestList
    var EmpIDX: Int = 0
    var RsecIDX: Int = 0
    var OrgIDX: Int = 0
    var URQIDX: Int = 0
    var unidx: Int = 0
    var acidx: Int = 0

    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnIt_Closejob_Save: Button
    private lateinit var btnIt_Closejob_Cancel: Button

    private lateinit var App_sap_lv1: Spinner
    private lateinit var App_sap_lv2: Spinner
    private lateinit var App_sap_lv3: Spinner
    private lateinit var App_sap_lv4: Spinner
    private lateinit var App_sap_lv5: Spinner
    private lateinit var sp_priolity: Spinner
    private lateinit var et_manhours: EditText
    private lateinit var et_sapmsg: EditText
    private lateinit var et_link: EditText
    private lateinit var et_comment: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_sap_closejob, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItSapCloseJobFragment {
            val fragment = ItSapCloseJobFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    fun SharedPreferences(){

        val settings: SharedPreferences = activity.getSharedPreferences(PREFS_NAME, 0)
        EmpIDX = settings.getInt("empIdx", 0).toString().toInt()
        RsecIDX = settings.getInt("rsecIdx", 0).toString().toInt()
        OrgIDX = settings.getInt("orgIdx", 0).toString().toInt()
    }

    private fun initInstances(rootView: View) {

        // TODO: Check intent value
        // Get data from intent
        userRequestList = activity.intent.getParcelableExtra("select")

        SharedPreferences()

        btnIt_Closejob_Save = rootView.findViewById(R.id.btnIt_Closejob_Save)
        btnIt_Closejob_Cancel = rootView.findViewById(R.id.btnIt_Closejob_Cancel)

        App_sap_lv1 = rootView.findViewById(R.id.sp_sap_lv1)
        App_sap_lv2 = rootView.findViewById(R.id.sp_sap_lv2)
        App_sap_lv3 = rootView.findViewById(R.id.sp_sap_lv3)
        App_sap_lv4 = rootView.findViewById(R.id.sp_sap_lv4)
        App_sap_lv5 = rootView.findViewById(R.id.sp_sap_lv5)
        sp_priolity = rootView.findViewById(R.id.sp_priolity)

        et_manhours = rootView.findViewById(R.id.et_manhours)
        et_sapmsg = rootView.findViewById(R.id.et_sapmsg)
        et_link = rootView.findViewById(R.id.et_link)
        et_comment = rootView.findViewById(R.id.et_comment)

        URQIDX = userRequestList.urqidx!!.toInt()
        unidx = userRequestList.unidx!!.toInt()
        acidx = userRequestList.acidx!!.toInt()

        btnIt_Closejob_Save.setOnClickListener(listener)
        btnIt_Closejob_Cancel.setOnClickListener(listener)

        select_sap_lv1()
        select_sap_lv3()
        select_sap_lv4()
        select_sap_lv5()
        spiner_status()
    }

    fun select_sap_lv1() {

        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        boxUserRequest.sysidx_add = 2
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_Select_ddlLV1(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()
                    //Log.d("Anser_user", "item = " + returnVal.toString())
                    //var newString = returnVal!!.replace(",\"ReturnCode\":[\"0\",\"0\"]", "")
                    val jsonElement_cl = gson.fromJson(returnVal, JsonObject::class.java)
                    val datalv_list = jsonElement_cl.get("DataSupportIT").asJsonObject

                    try {

                        val dao_lv = gson.fromJson(datalv_list, DataSupportIT::class.java)
                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")
                        val datauser_array = dao_lv.BoxUserRequest

                        if (datauser_array != null) {
                            for (a in datauser_array) {

                                lv.add(a.name_code1.toString())
                            }
                        }

                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_sap_lv1.adapter = adapter
                        App_sap_lv1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()

                                var name: String = App_sap_lv1.selectedItem.toString()
                                Log.d("App_createuser", "namelv1 = " + name)
                                if (name != "กรุณาเลือกข้อมูล") {
                                    select_sap_lv2(name)
                                }
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }

                    catch (e: Exception) {

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")
                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_sap_lv1.adapter = adapter
                        App_sap_lv1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }

                    }

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    fun select_sap_lv2(name: String) {

        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        //boxUserRequest.sysidx_add = 2
        boxUserRequest.chkadaccept = name
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_Select_ddlLV2(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()
                    var newString = returnVal!!.replace(",\"SysIDX\":[\"2\",\"2\"]", "")
                    val jsonElement2 = gson.fromJson(newString, JsonObject::class.java)
                    val datalv2_list = jsonElement2.get("DataSupportIT").asJsonObject
                    Log.d("App_lv2", "item list = " + returnVal.toString())

                    try {

                        val dao_lv = gson.fromJson(datalv2_list, DataSupportIT::class.java)

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")

                        val datalv_array = dao_lv.BoxUserRequest

                        Log.d("Anser_user", "item list = " + datalv_array.toString())

                        if (datalv_array != null) {
                            for (a in datalv_array) {
                                lv.add(a.name_code2.toString())
                            }
                        }

                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_sap_lv2.adapter = adapter
                        App_sap_lv2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()

                                var name2: String = App_sap_lv1.selectedItem.toString()
                                if (name2 != "กรุณาเลือกข้อมูล") {
                                    select_sap_lv3()
                                }
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }

                    catch (e: Exception) {

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")
                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_sap_lv2.adapter = adapter
                        App_sap_lv2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    fun select_sap_lv3() {

        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        boxUserRequest.sysidx_add = 2
        //boxUserRequest.chkadaccept = name
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_Select_ddlLV3(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val datalv2_list = jsonElement2.get("DataSupportIT").asJsonObject

                    try {

                        val dao_lv = gson.fromJson(datalv2_list, DataSupportIT::class.java)

                        //Log.d("App_lv3", "item list = " + datalv2_list)

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")

                        val datalv_array = dao_lv.BoxUserRequest

                        Log.d("Anser_user", "item list = " + datalv_array.toString())

                        if (datalv_array != null) {
                            for (a in datalv_array) {

                                lv.add(a.name_code3.toString())
                            }
                        }

                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_sap_lv3.adapter = adapter
                        App_sap_lv3.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()

                                /*var name: String = App_sap_lv1.selectedItem.toString()
                                Log.d("App_createuser", "item list = " + name)
                                if (name != "กรุณาเลือกข้อมูล") {
                                    select_sap_lv4()
                                }*/
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }

                    catch (e: Exception) {

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")
                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_sap_lv3.adapter = adapter
                        App_sap_lv3.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }

    fun select_sap_lv4() {

        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        boxUserRequest.sysidx_add = 2
        //boxUserRequest.chkadaccept = name
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_Select_ddlLV4(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val datalv2_list = jsonElement2.get("DataSupportIT").asJsonObject

                    try {

                        val dao_lv = gson.fromJson(datalv2_list, DataSupportIT::class.java)

                        //Log.d("App_lv3", "item list = " + datalv2_list)

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")

                        val datalv_array = dao_lv.BoxUserRequest

                        Log.d("Anser_user", "item list = " + datalv_array.toString())

                        if (datalv_array != null) {
                            for (a in datalv_array) {

                                lv.add(a.name_code4.toString())
                            }
                        }

                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_sap_lv4.adapter = adapter
                        App_sap_lv4.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()

                                /*var name: String = App_sap_lv1.selectedItem.toString()
                                Log.d("App_createuser", "item list = " + name)
                                if (name != "กรุณาเลือกข้อมูล") {
                                    select_sap_lv5()
                                }*/
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }

                    catch (e: Exception) {

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")
                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_sap_lv4.adapter = adapter
                        App_sap_lv4.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }

    fun select_sap_lv5() {

        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        boxUserRequest.sysidx_add = 2
        //boxUserRequest.chkadaccept = name
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_Select_ddlLV5(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val datalv2_list = jsonElement2.get("DataSupportIT").asJsonObject

                    try {

                        val dao_lv = gson.fromJson(datalv2_list, DataSupportIT::class.java)

                        //Log.d("App_lv3", "item list = " + datalv2_list)

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")

                        val datalv_array = dao_lv.BoxUserRequest

                        Log.d("Anser_user", "item list = " + datalv_array.toString())

                        if (datalv_array != null) {
                            for (a in datalv_array) {

                                lv.add(a.name_code5.toString())
                            }
                        }

                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_sap_lv5.adapter = adapter
                        App_sap_lv5.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }

                    catch (e: Exception) {

                        val lv: MutableList<String> = mutableListOf()
                        lv.add(0, "กรุณาเลือกข้อมูล")
                        val readOnlyView_lv: kotlin.collections.List<String> = lv

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_lv)
                        App_sap_lv5.adapter = adapter
                        App_sap_lv5.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_lv[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }
                    }

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }

    fun spiner_status() {

        val status: MutableList<String> = mutableListOf()
        val readOnlyView_status: kotlin.collections.List<String> = status

        status.add("ด่วน")
        status.add("ไม่ด่วน")
        status.add("ปานกลาง")
        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_status)
        sp_priolity.adapter = adapter
        sp_priolity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                Toast.makeText(getActivity(), readOnlyView_status[position], Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }
    }


    fun urlUpdate() {

        var pidx_idx: Int = 0
        var staidx: Int = 0

        //var createuser: String = App_createuser.selectedItem.toString()

        if(unidx == 2 && acidx == 1){
            /*if(approve == 5){
                staidx = 24
            }
            else{*/
                staidx = 23
            //}
        }

        var priolity: String = sp_priolity.selectedItem.toString()

        if(priolity == "ด่วน"){
            pidx_idx = 1
        }
        else if(priolity == "ไม่ด่วน"){
            pidx_idx = 2
        }
        else{
            pidx_idx = 3
        }

        var dataSupportIT = DataSupportIT()
        var userRequestList = UserRequestList()
        userRequestList.urqidx = URQIDX
        userRequestList.adminidx = EmpIDX

        userRequestList.unidx = unidx
        userRequestList.acidx = acidx
        userRequestList.pidx_add = pidx_idx
        userRequestList.staidx = staidx

        dataSupportIT.BoxUserRequest = arrayListOf(userRequestList)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("DataSupportIT", "jsonIn = " + jsonIn)

        var call = HttpManager.getInstance().service.getItServicesList_Update_SapGetJob(jsonIn)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("DataSupportIT", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupport = jsonElement2.get("DataSupportIT").asJsonObject

                } else {
                    Log.d("DataSupportIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    fun urlCloseJob_Admin() {

        var pidx_idx: Int = 0
        var priolity: String = sp_priolity.selectedItem.toString()

        if(priolity == "ด่วน"){
            pidx_idx = 1
        }
        else if(priolity == "ไม่ด่วน"){
            pidx_idx = 2
        }
        else{
            pidx_idx = 3
        }


        var dataSupportIT = DataSupportIT()
        var userRequestList = UserRequestList()
        userRequestList.urqidx = URQIDX
        userRequestList.ms2idx = 0
        userRequestList.ms3idx = 0
        userRequestList.ms4idx = 0

        userRequestList.name_code1 = App_sap_lv1.selectedItem.toString()
        userRequestList.name_code2 = App_sap_lv2.selectedItem.toString()
        userRequestList.name_code3 = App_sap_lv3.selectedItem.toString()
        userRequestList.name_code4 = App_sap_lv4.selectedItem.toString()
        userRequestList.name_code5 = App_sap_lv5.selectedItem.toString()

        userRequestList.pidx_add = pidx_idx
        userRequestList.manhours = et_manhours.text.toString().toInt()
        userRequestList.link = et_link.text.toString()
        userRequestList.sapmsg = et_sapmsg.text.toString()

        userRequestList.adminidx = EmpIDX
        userRequestList.admindoingidx = EmpIDX
        userRequestList.commentamdoing = et_comment.text.toString()
        userRequestList.ccaidx = 0
        userRequestList.recieveidx = 0
        userRequestList.empidx_add = EmpIDX
        userRequestList.unidx = unidx
        userRequestList.staidx = 1

        dataSupportIT.BoxUserRequest = arrayListOf(userRequestList)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("DataSupportIT", "jsonIn = " + jsonIn)

        var call = HttpManager.getInstance().service.getItServicesList_Sap_CloseJob(jsonIn)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("DataSupportIT", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupport = jsonElement2.get("DataSupportIT").asJsonObject

                } else {
                    Log.d("DataSupportIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when(view.id) {
            R.id.btnIt_Closejob_Save -> {
                if(et_comment.text.toString() == "" || App_sap_lv1.selectedItem.toString() == "กรุณาเลือกข้อมูล" ||
                        App_sap_lv2.selectedItem.toString() == "กรุณาเลือกข้อมูล" || App_sap_lv3.selectedItem.toString() == "กรุณาเลือกข้อมูล" ||
                        App_sap_lv4.selectedItem.toString() == "กรุณาเลือกข้อมูล" || App_sap_lv5.selectedItem.toString() == "กรุณาเลือกข้อมูล" ||
                        sp_priolity.selectedItem.toString() == "กรุณาเลือกข้อมูล" || et_manhours.text.toString() == "" ||
                        et_sapmsg.text.toString() == "" || et_link.text.toString() == ""){

                    val simpleAlert = AlertDialog.Builder(getActivity()).create()
                    simpleAlert.setTitle("แจ้งเตือน")
                    simpleAlert.setMessage("กรุณากรอกข้อมูลให้ครบถ้วน")

                    simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", { dialogInterface, i ->

                    })

                    simpleAlert.show()
                    Log.d("Insert", "No = ไม่สำเร็จ")

                }
                else{

                    urlUpdate()
                    urlCloseJob_Admin()

                    Log.d("Insert", "OK = สำเร็จ")

                    var itItList = Intent(activity, ItSapListActivity::class.java)
                    startActivity(itItList)
                }

            }
            R.id.btnIt_Closejob_Cancel -> {
                var itItList = Intent(activity, ItSapListActivity::class.java)
                startActivity(itItList)
            }
        }
    }
}