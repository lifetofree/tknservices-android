package tkn.dev.tknservices.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItGoogleListActivity
import tkn.dev.tknservices.activity.ItItListActivity
import tkn.dev.tknservices.activity.ItPosListActivity
import tkn.dev.tknservices.activity.ItSapListActivity

/**
 * Created by lifetofree on 10/19/17.
 */
class CentralizedFragment : Fragment() {

    var someVar: Int = 0

    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnCenItIt: com.inthecheesefactory.thecheeselibrary.widget.AdjustableImageButton
    private lateinit var btnCenItSap: com.inthecheesefactory.thecheeselibrary.widget.AdjustableImageButton
    private lateinit var btnCenItGoogle: com.inthecheesefactory.thecheeselibrary.widget.AdjustableImageButton
    private lateinit var btnCenItPos: com.inthecheesefactory.thecheeselibrary.widget.AdjustableImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_centralized, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): CentralizedFragment {
            val fragment = CentralizedFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    private fun initInstances(rootView: View) {
        // Init 'View' instance(s) with rootView.findViewById here
        tvCustomTitleBar = activity.findViewById(R.id.tvCustomTitleBar)
        tvCustomTitleBar.text = "mas"

        btnCenItIt = rootView.findViewById(R.id.btnCenItIt)
        btnCenItSap = rootView.findViewById(R.id.btnCenItSap)
        btnCenItGoogle = rootView.findViewById(R.id.btnCenItGoogle)
        btnCenItPos = rootView.findViewById(R.id.btnCenItPos)

        btnCenItIt.setOnClickListener(listener)
        btnCenItSap.setOnClickListener(listener)
        btnCenItGoogle.setOnClickListener(listener)
        btnCenItPos.setOnClickListener(listener)
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when(view.id) {
            R.id.btnCenItIt -> {
                var itItList = Intent(activity, ItItListActivity::class.java)
                startActivity(itItList)
            }
            R.id.btnCenItSap -> {
                var itSapList = Intent(activity, ItSapListActivity::class.java)
                startActivity(itSapList)
            }
            R.id.btnCenItGoogle -> {
                var itGoogleList = Intent(activity, ItGoogleListActivity::class.java)
                startActivity(itGoogleList)
            }
            R.id.btnCenItPos -> {
                var itPosList = Intent(activity, ItPosListActivity::class.java)
                startActivity(itPosList)
            }
        }
    }
}