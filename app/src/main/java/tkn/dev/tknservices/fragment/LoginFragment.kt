package tkn.dev.tknservices.fragment

import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.R
import tkn.dev.tknservices.manager.HttpManager
import java.io.UnsupportedEncodingException
import java.security.NoSuchAlgorithmException
import okhttp3.ResponseBody
import tkn.dev.tknservices.dao.DataEmployee
import tkn.dev.tknservices.dao.EmployeeDetail
import tkn.dev.tknservices.manager.Contextor


/**
 * Created by lifetofree on 10/19/17.
 */
class LoginFragment : Fragment() {

    private val PREFS_NAME: String = "EmployeePrefs"
    var someVar: Int = 0

    private lateinit var btnLogin: Button
    private lateinit var editTextUsername: EditText
    private lateinit var editTextPassword: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_login, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): LoginFragment {
            val fragment = LoginFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("empIDX", empIDX)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    private fun initInstances(rootView: View) {
        // Init 'View' instance(s) with rootView.findViewById here
        btnLogin = rootView.findViewById(R.id.btnLogin)
        editTextUsername = rootView.findViewById(R.id.editTextUsername)
        editTextPassword = rootView.findViewById(R.id.editTextPassword)

        btnLogin.setOnClickListener(listener)

    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }


    var listener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.btnLogin -> {
                var sUsername: String = editTextUsername.text.toString()
                var sPassword: String = editTextPassword.text.toString()

                // Check Empty Value
                if (sUsername == null || sUsername == "" || sPassword == null || sPassword == "") {
                    val alert = AlertDialog.Builder(this.activity)
                    alert.setTitle("แจ้งเตือน")
                    alert.setMessage("กรุณากรอกข้อมูลให้ครบถ้วน")
                    alert.setPositiveButton("OK", DialogInterface.OnClickListener { _: DialogInterface, _: Int ->
                        fun onClick(dialog: DialogInterface, index: Int) {
                            dialog.dismiss()
                        }
                    })
                    alert.show()
                } else {
                    var dataEmployee = DataEmployee()
                    var employeeDetail = EmployeeDetail()
                    employeeDetail.empCode = sUsername//"56000088"
                    employeeDetail.empPassword = md5(sPassword)//"53d9d48078580fbf6a0fd139c2622bba"
                    dataEmployee.employeeList = arrayListOf(employeeDetail)

                    val gson = GsonBuilder().create()

                    val jsonElement: JsonElement = gson.toJsonTree(dataEmployee)
                    val jsonObject = JsonObject()
                    jsonObject.add("data_employee", jsonElement)
                    val jsonIn = jsonObject.toString()

                    Log.d("Employee", "jsonIn = " + jsonIn)

                    var call = HttpManager.getInstance().service.checkEmployee(jsonIn)
                    call.enqueue(object : Callback<ResponseBody?> {
                        override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                            if (response.isSuccessful) {
                                val returnVal = response.body()?.string()

                                Log.d("Employee", "success body = " + returnVal)

                                val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                                val dataEmployee = jsonElement2.get("data_employee").asJsonObject

                                val returnCode = dataEmployee.get("return_code").asInt
                                Log.d("Employee", "return code = " + returnCode.toString())
                                val returnMsg = dataEmployee.get("return_msg").asString

                                if (returnCode == 0) {
                                    val resultEmployee = dataEmployee.get("employee_list")
                                    val employeeList = gson.fromJson(resultEmployee, EmployeeDetail::class.java)

                                    Log.d("Employee", "employee list = " + employeeList.toString())

                                    // Set SharedPreferences
                                    val settings: SharedPreferences = activity.getSharedPreferences(PREFS_NAME, 0)
                                    val editor: SharedPreferences.Editor = settings.edit()
                                    editor.putInt("empIdx", employeeList.empIdx!!)
                                    editor.putInt("rsecIdx", employeeList.rsecIdx!!)
                                    editor.putInt("orgIdx", employeeList.orgIdx!!)
                                    //editor.putString("empNameTH", employeeList.empNameTh!!)
                                    editor.commit()

                                    Log.d("Employee", "empIdx = " + settings.getInt("empIdx", 0).toString())

                                    activity.supportFragmentManager.beginTransaction()
                                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                            .replace(R.id.contentContainer,
                                                    CentralizedFragment.newInstance())
                                            .commit()
                                } else {
                                    Log.d("Employee", "return msg = " + returnMsg)

                                    Toast.makeText(activity,
                                            "รหัสพนักงาน หรือ รหัสผ่านไม่ถูกต้อง",
                                            Toast.LENGTH_SHORT)
                                            .show()
                                }
                            } else {
                                Log.d("Employee", "error body = " + response.errorBody()?.string())
                            }
                        }

                        override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                            Log.d("Employee", "failure = " + t.toString())
                        }
                    })
                }
            }
        }
    }

    private fun md5(input: String): String {
        try {
            val md = java.security.MessageDigest.getInstance("MD5")
            val array = md.digest(input.toByteArray(charset("UTF-8")))
            val sb = StringBuffer()
            for (i in array.indices) {
                sb.append(String.format("%02x", array[i]))
            }
            return sb.toString()
        } catch (e: NoSuchAlgorithmException) {
            return ""
        } catch (e: UnsupportedEncodingException) {
            return ""
        }
    }
}