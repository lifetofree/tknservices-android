package tkn.dev.tknservices.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItGoogleListActivity
import tkn.dev.tknservices.activity.ItItListActivity

/**
 * Created by lifetofree on 10/19/17.
 */
class ItGoogleCommentFragment : Fragment() {

    var someVar: Int = 0

    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnItGoogleList: Button
    private lateinit var btnItGoogleCancel: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_google_comment, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItGoogleCommentFragment {
            val fragment = ItGoogleCommentFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    private fun initInstances(rootView: View) {
        // Init 'View' instance(s) with rootView.findViewById here
        //tvCustomTitleBar = activity.findViewById(R.id.tvCustomTitleBar)
        //tvCustomTitleBar.text = "google comment"
        //tvCustomTitleBar.setBackgroundResource(R.drawable.bg_title_it_google)
        btnItGoogleList = rootView.findViewById(R.id.btnItGoogleList)
        btnItGoogleCancel = rootView.findViewById(R.id.btnItGoogleCancel)

        btnItGoogleList.setOnClickListener(listener)
        btnItGoogleCancel.setOnClickListener(listener)
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
//            R.id.btnItGoogleCreate -> {
//                var itItList = Intent(activity, ItGoogleCreateActivity::class.java)
//                startActivity(itItList)
//            }
            R.id.btnItGoogleList -> {
                var itItList = Intent(activity, ItGoogleListActivity::class.java)
                startActivity(itItList)
            }
            R.id.btnItGoogleCancel -> {
                var itItCancel = Intent(activity, ItGoogleListActivity::class.java)
                startActivity(itItCancel)
            }
        }
    }
}