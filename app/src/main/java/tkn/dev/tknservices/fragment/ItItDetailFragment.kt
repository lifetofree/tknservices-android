package tkn.dev.tknservices.fragment

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.*
import tkn.dev.tknservices.dao.DataSupportIT
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.manager.HttpManager

/**
 * Created by lifetofree on 10/19/17.
 */
class ItItDetailFragment : Fragment() {

    var someVar: Int = 0
    private val PREFS_NAME: String = "EmployeePrefs"
    var EmpIDX: Int = 0
    var EmpIDX_create: Int = 0
    var RsecIDX: Int = 0
    var URQIDX: Int = 0
    var unidx: Int = 0
    var acidx: Int = 0
    var RDeptIDX: Int = 0

    private val TAG = "Tab1Fragment"
    private lateinit var userRequestList: UserRequestList

    private lateinit var tvDocCode: TextView
    private lateinit var tvCreateDateTime: TextView
    private lateinit var tvEmpName_Create: TextView
    private lateinit var tvTel: TextView
    private lateinit var tvLocation: TextView
    private lateinit var tvRemoteName: TextView
    private lateinit var tvUserRemote: TextView
    private lateinit var tvPassRemote: TextView
    private lateinit var tvComment: TextView


    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnItSave: Button
    private lateinit var btnItCloseJob: Button
    private lateinit var btnItTransfer: Button
    private lateinit var btnItUserCheck: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_it_detail, container, false)
        initInstances(rootView)

        return rootView

    }

    companion object {
        fun newInstance(): ItItDetailFragment {
            val fragment = ItItDetailFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    fun SharedPreferences(){

        val settings: SharedPreferences = activity.getSharedPreferences(PREFS_NAME, 0)
        EmpIDX = settings.getInt("empIdx", 0).toString().toInt()
        RsecIDX = settings.getInt("rsecIdx", 0).toString().toInt()
    }

    private fun initInstances(rootView: View) {

        // TODO: Check intent value
        // Get data from intent
        userRequestList = activity.intent.getParcelableExtra("selectedData")

        SharedPreferences()

        btnItSave = rootView.findViewById(R.id.btnItSave)
        btnItCloseJob = rootView.findViewById(R.id.btnItCloseJob)
        btnItTransfer = rootView.findViewById(R.id.btnItTransfer)
        btnItUserCheck = rootView.findViewById(R.id.btnItUserCheck)

        tvDocCode = rootView.findViewById(R.id.tvDocCode)
        tvCreateDateTime = rootView.findViewById(R.id.tvCreateDateTime)
        tvEmpName_Create = rootView.findViewById(R.id.tvEmpName_Create)
        tvTel = rootView.findViewById(R.id.tvTel)
        tvLocation = rootView.findViewById(R.id.tvLocation)
        tvRemoteName = rootView.findViewById(R.id.tvRemoteName)
        tvUserRemote = rootView.findViewById(R.id.tvUserRemote)
        tvPassRemote = rootView.findViewById(R.id.tvpass)
        tvComment = rootView.findViewById(R.id.tvComment)

        tvDocCode.text = userRequestList.doccode.toString()
        tvCreateDateTime.text = userRequestList.createdateuser.toString() + " " + userRequestList.timecreatejob.toString()
        tvEmpName_Create.text = userRequestList.empname.toString()
        tvTel.text = userRequestList.teletc.toString()
        tvLocation.text = userRequestList.locname.toString()
        tvRemoteName.text = userRequestList.remotename.toString()
        tvUserRemote.text = userRequestList.useridremote.toString()
        tvPassRemote.text = userRequestList.passwordremote.toString()
        tvComment.text = userRequestList.detailuser.toString()

        URQIDX = userRequestList.urqidx!!.toInt()
        unidx = userRequestList.unidx!!.toInt()
        acidx = userRequestList.acidx!!.toInt()
        RDeptIDX = userRequestList.rdeptidx!!.toInt()
        EmpIDX_create = userRequestList.empidx!!.toInt()

        btnItSave.setOnClickListener(listener)
        btnItCloseJob.setOnClickListener(listener)
        btnItTransfer.setOnClickListener(listener)
        btnItUserCheck.setOnClickListener(listener)

        Permission_button()
    }

    fun Permission_button(){
        Log.d("unidx", "success = " + unidx)
        Log.d("acidx", "success = " + acidx)
        Log.d("EmpIDX_create", "success = " + EmpIDX_create)
        Log.d("EmpIDX", "success = " + EmpIDX)
        Log.d("RDeptIDX", "success = " + RDeptIDX)

        // View.INVISIBLE ซ่อน
        // View.VISIBLE แสดง

        if(acidx == 1) { //ผู้สร้าง
            if (unidx == 2 && EmpIDX == EmpIDX_create) {
                btnItSave.visibility = View.GONE
                btnItTransfer.visibility = View.GONE
                btnItCloseJob.visibility = View.GONE
                btnItUserCheck.visibility = View.VISIBLE
            } else {
                btnItSave.visibility = View.GONE
                btnItTransfer.visibility = View.GONE
                btnItCloseJob.visibility = View.GONE
                btnItUserCheck.visibility = View.GONE
            }
        }
        else if(acidx == 3) { //เจ้าหน้าที่ IT
            if (unidx == 7 && (RDeptIDX == 20 || RDeptIDX == 21)) {
                btnItSave.visibility = View.VISIBLE
                btnItTransfer.visibility = View.VISIBLE
                btnItCloseJob.visibility = View.GONE
                btnItUserCheck.visibility = View.GONE
            } else if (unidx == 4 && (RDeptIDX == 20 || RDeptIDX == 21)) {
                btnItSave.visibility = View.GONE
                btnItTransfer.visibility = View.GONE
                btnItCloseJob.visibility = View.VISIBLE
                btnItUserCheck.visibility = View.GONE
            }
        }
        else if(acidx == 2) {
            if (unidx == 3 && (RDeptIDX == 20 || RDeptIDX == 21)) {
                btnItSave.visibility = View.GONE
                btnItTransfer.visibility = View.GONE
                btnItCloseJob.visibility = View.VISIBLE
                btnItUserCheck.visibility = View.GONE
            }
        }
        else{
            btnItSave.visibility = View.GONE
            btnItTransfer.visibility = View.GONE
            btnItCloseJob.visibility = View.GONE
            btnItUserCheck.visibility = View.GONE
        }
    }

    fun update_receive() {

        var PIDX_Add: Int = 0
        var staidx: Int = 0

        //var statusreceive: String = App_status.selectedItem.toString()

        /**************************************************/
        if(unidx == 2 && acidx == 1){
            /*if(approve == 5){
                staidx = 24
            }
            else{*/
            staidx = 23
            //}
        }

        var dataSupportIT = DataSupportIT()
        var userRequestList = UserRequestList()
        userRequestList.urqidx = URQIDX
        userRequestList.adminidx = EmpIDX

        userRequestList.unidx = unidx
        userRequestList.acidx = acidx
        userRequestList.staidx = staidx


        dataSupportIT.BoxUserRequest = arrayListOf(userRequestList)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("DataSupportIT", "jsonIn = " + jsonIn)

        var call = HttpManager.getInstance().service.getItServicesList_Update_ITGetJob(jsonIn)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("DataSupportIT", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupport = jsonElement2.get("DataSupportIT").asJsonObject

                } else {
                    Log.d("DataSupportIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when(view.id) {
            R.id.btnItSave -> {

                update_receive()

                var itItList = Intent(activity, ItItListActivity::class.java)
                startActivity(itItList)
            }
            R.id.btnItCloseJob -> {
                //TODO: set value

                val itItGoogleCreate = Intent(activity, ItItCreateActivity::class.java)
                // Set fragmentName
                itItGoogleCreate.putExtra("fragmentName", "ItItReceive")
                itItGoogleCreate.putExtra("select", userRequestList)
                startActivity(itItGoogleCreate)
            }
            R.id.btnItTransfer -> {
                //TODO: set value

                val itItGoogleCreate = Intent(activity, ItItCreateActivity::class.java)
                // Set fragmentName
                itItGoogleCreate.putExtra("fragmentName", "ItItJobTranfer")
                itItGoogleCreate.putExtra("select", userRequestList)
                startActivity(itItGoogleCreate)
            }
            R.id.btnItUserCheck -> {
                //TODO: set value

                val itItGoogleCreate = Intent(activity, ItItCreateActivity::class.java)
                // Set fragmentName
                itItGoogleCreate.putExtra("fragmentName", "ItItUserCloseJob")
                itItGoogleCreate.putExtra("select", userRequestList)
                startActivity(itItGoogleCreate)
            }
        }
    }
}