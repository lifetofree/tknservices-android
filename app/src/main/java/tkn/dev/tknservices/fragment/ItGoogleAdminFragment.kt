package tkn.dev.tknservices.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.R
import tkn.dev.tknservices.dao.DataSupportIT
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.manager.HttpManager

/**
 * Created by lifetofree on 10/19/17.
 */
class ItGoogleAdminFragment : Fragment() {

    var someVar: Int = 0

    private lateinit var userRequestList: UserRequestList
    private lateinit var tvCustomTitleBar: TextView

    private lateinit var tvDateReciveJobFirst: TextView
    private lateinit var tvsumtime: TextView
    private lateinit var tvDateCloseJob: TextView
    private lateinit var tvStatus: TextView
    private lateinit var tvAdminName: TextView
    private lateinit var tvAdminDoingName: TextView
    private lateinit var tvCommentAMDoing: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_google_admin, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItGoogleAdminFragment {
            val fragment = ItGoogleAdminFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    private fun initInstances(rootView: View) {
        // Init 'View' instance(s) with rootView.findViewById here

        // TODO: Check intent value
        // Get data from intent
        userRequestList = activity.intent.getParcelableExtra("selectedData")

        tvDateReciveJobFirst = rootView.findViewById(R.id.tvDateReciveJobFirst)
        tvsumtime = rootView.findViewById(R.id.tvsumtime)
        tvDateCloseJob = rootView.findViewById(R.id.tvDateCloseJob)
        tvStatus = rootView.findViewById(R.id.tvStatus)
        tvAdminName = rootView.findViewById(R.id.tvAdminName)
        tvAdminDoingName = rootView.findViewById(R.id.tvAdminDoingName)
        tvCommentAMDoing = rootView.findViewById(R.id.tvCommentAMDoing)

        select_admin()
    }

    fun select_admin() {

        //Log.d("Employee", "urqidx = " + userRequestList.urqidx.toString())

        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        boxUserRequest.fileuser = 0
        boxUserRequest.urqidx = userRequestList.urqidx.toString().toInt()
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_Select_DetailCloseJobList(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()
                    var newString = returnVal!!.replace(",\"CheckRemote\":[\"0\",\"0\"]", "")
                    var newString1 = newString!!.replace(",\"CheckRemote\":[\"1\",\"1\"]", "")
                    val jsonElement2 = gson.fromJson(newString1, JsonObject::class.java)

                    val datauser_list = jsonElement2.get("DataSupportIT").asJsonObject

                    try {

                        val resultEmployee = datauser_list.get("BoxUserRequest")
                        val employeeList = gson.fromJson(resultEmployee, UserRequestList::class.java)

                        tvDateReciveJobFirst.text = employeeList.daterecivejobfirst.toString() + " " + employeeList.timerecivejobfirst.toString()
                        tvsumtime.text = employeeList.sumtime.toString()
                        tvDateCloseJob.text = employeeList.dateclosejob.toString() + " " + employeeList.timeclosejob.toString()
                        tvStatus.text = employeeList.staname.toString()
                        tvAdminName.text = employeeList.adminname.toString()
                        tvAdminDoingName.text = employeeList.admindoingname.toString()
                        tvCommentAMDoing.text = employeeList.commentamdoing.toString()

                    }

                    catch (e: Exception) {

                    }

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
//        when (view.id) {
//            R.id.btnItGoogleCreate -> {
//                var itItList = Intent(activity, ItGoogleCreateActivity::class.java)
//                startActivity(itItList)
//            }
//        }
    }
}