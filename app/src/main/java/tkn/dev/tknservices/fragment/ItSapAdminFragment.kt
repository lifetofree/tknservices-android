package tkn.dev.tknservices.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.R
import tkn.dev.tknservices.dao.DataSupportIT
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.manager.HttpManager

class ItSapAdminFragment : Fragment() {

    var someVar: Int = 0
    private lateinit var userRequestList: UserRequestList

    private lateinit var tvDateReciveJobFirst: TextView
    private lateinit var tvlv1: TextView
    private lateinit var tvlv2: TextView
    private lateinit var tvlv3: TextView
    private lateinit var tvlv4: TextView
    private lateinit var tvlv5: TextView

    private lateinit var tvPriority_name: TextView
    private lateinit var tvManHours: TextView
    private lateinit var tvsumtime: TextView
    private lateinit var tvLink: TextView
    private lateinit var tvSapmsg: TextView
    private lateinit var tvDateCloseJob: TextView
    private lateinit var tvStaName: TextView
    private lateinit var tvAdminName: TextView
    private lateinit var tvAdminDoingName: TextView
    private lateinit var tvCommentAMDoing: TextView

    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnItItList: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_sap_admin, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItSapAdminFragment {
            val fragment = ItSapAdminFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    private fun initInstances(rootView: View) {

        // TODO: Check intent value
        // Get data from intent
        userRequestList = activity.intent.getParcelableExtra("selectedData")

        tvDateReciveJobFirst = rootView.findViewById(R.id.tvDateReciveJobFirst)
        tvlv1 = rootView.findViewById(R.id.tvlv1)
        tvlv2 = rootView.findViewById(R.id.tvlv2)
        tvlv3 = rootView.findViewById(R.id.tvlv3)
        tvlv4 = rootView.findViewById(R.id.tvlv4)
        tvlv5 = rootView.findViewById(R.id.tvlv5)
        tvPriority_name = rootView.findViewById(R.id.tvPriority_name)
        tvManHours = rootView.findViewById(R.id.tvManHours)
        tvsumtime = rootView.findViewById(R.id.tvsumtime)
        tvLink = rootView.findViewById(R.id.tvLink)
        tvSapmsg = rootView.findViewById(R.id.tvSapmsg)
        tvDateCloseJob = rootView.findViewById(R.id.tvDateCloseJob)
        tvStaName = rootView.findViewById(R.id.tvStaName)
        tvAdminName = rootView.findViewById(R.id.tvAdminName)
        tvAdminDoingName = rootView.findViewById(R.id.tvAdminDoingName)
        tvCommentAMDoing = rootView.findViewById(R.id.tvCommentAMDoing)

        select_admin()
    }

    fun select_admin() {

        Log.d("Employee", "urqidx = " + userRequestList.urqidx.toString())

        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        boxUserRequest.fileuser = 0
        boxUserRequest.urqidx = userRequestList.urqidx.toString().toInt()
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_Select_DetailCloseJobList(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()
                    var newString = returnVal!!.replace(",\"CheckRemote\":[\"0\",\"0\"]", "")
                    var newString1 = newString!!.replace(",\"CheckRemote\":[\"1\",\"1\"]", "")
                    val jsonElement_s = gson.fromJson(newString1, JsonObject::class.java)

                    val datauser_list = jsonElement_s.get("DataSupportIT").asJsonObject



                    try {

                        val resultEmployee = datauser_list.get("BoxUserRequest")
                        val employeeList = gson.fromJson(resultEmployee, UserRequestList::class.java)

                        //Log.d("M", "body = " + employeeList.doccode.toString())

                        tvDateReciveJobFirst.text = employeeList.daterecivejobfirst.toString() + " " + employeeList.timerecivejobfirst.toString()
                        tvlv1.text = employeeList.name1.toString()
                        tvlv2.text = employeeList.name2.toString()
                        tvlv3.text = employeeList.name3.toString()
                        tvlv4.text = employeeList.name4.toString()
                        tvlv5.text = employeeList.name5.toString()
                        tvPriority_name.text = employeeList.Priority_name.toString()
                        tvManHours.text = employeeList.manhours.toString()
                        tvsumtime.text = employeeList.sumtime.toString()
                        tvLink.text = employeeList.link.toString()
                        tvSapmsg.text = employeeList.sapmsg.toString()
                        tvDateCloseJob.text = employeeList.dateclosejob.toString() + " " + employeeList.timeclosejob.toString()
                        tvStaName.text = employeeList.staname.toString()
                        tvAdminName.text = employeeList.adminname.toString()
                        tvAdminDoingName.text = employeeList.admindoingname.toString()
                        tvCommentAMDoing.text = employeeList.commentamdoing.toString()

                    }

                    catch (e: Exception) {

                    }

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when(view.id) {
            R.id.btnItItList -> {
                //var itItList = Intent(activity, ItItDetailActivity::class.java)
                //startActivity(itItList)

            }
        }
    }
}