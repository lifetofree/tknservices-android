package tkn.dev.tknservices.fragment

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItGoogleListActivity
import tkn.dev.tknservices.activity.ItItListActivity
import tkn.dev.tknservices.dao.DataSupportIT
import tkn.dev.tknservices.dao.EmployeeDetail
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.dao.data_employeelist
import tkn.dev.tknservices.manager.HttpManager

/**
 * Created by lifetofree on 10/19/17.
 */
class ItGoogleCreateFragment : Fragment() {

    var someVar: Int = 0
    var emp_name: String = ""
    private val PREFS_NAME: String = "EmployeePrefs"
    var EmpIDX: Int = 0
    var RsecIDX: Int = 0

    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnGoogleCreateSave: Button
    private lateinit var btnGoogleCreateCancel: Button

    private lateinit var App_createuser: Spinner
    private lateinit var App_location: Spinner
    private lateinit var et_tel: EditText
    private lateinit var et_comment: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_google_create, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItGoogleCreateFragment {
            val fragment = ItGoogleCreateFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    fun SharedPreferences(){

        val settings: SharedPreferences = activity.getSharedPreferences(PREFS_NAME, 0)
        EmpIDX = settings.getInt("empIdx", 0).toString().toInt()
        RsecIDX = settings.getInt("rsecIdx", 0).toString().toInt()
    }

    private fun initInstances(rootView: View) {

        SharedPreferences()
        // Find Button
        btnGoogleCreateSave = rootView.findViewById(R.id.btnGoogleCreateSave)
        btnGoogleCreateCancel = rootView.findViewById(R.id.btnGoogleCreateCancel)

        App_createuser = rootView.findViewById(R.id.sp_createname)
        App_location = rootView.findViewById(R.id.sp_location)
        et_tel = rootView.findViewById(R.id.et_tel)
        et_comment = rootView.findViewById(R.id.et_comment)

        // Add Listener
        btnGoogleCreateSave.setOnClickListener(listener)
        btnGoogleCreateCancel.setOnClickListener(listener)

        select_defult_user()
        select_user()
        select_location()
    }

    fun select_defult_user(){

        val gson = GsonBuilder().create()

        var call = HttpManager.getInstance().service.getEmployee(EmpIDX) //คอยข้อมูลจาก Login
        call.enqueue(object: Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("Employee", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataEmployee = jsonElement2.get("data_employee").asJsonObject

                    val resultEmployee = dataEmployee.get("employee_list")
                    val employeeList = gson.fromJson(resultEmployee, EmployeeDetail::class.java)

                    val datauser_array = employeeList.empNameTh

                    Log.d("Employee", "empNameTH = " + datauser_array.toString())
                    emp_name = datauser_array.toString()

                } else {
                    Log.d("Employee", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }

    fun select_user() {

        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        boxUserRequest.rsecid = RsecIDX //web
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()


        var call = HttpManager.getInstance().service.getItServicesList_SelectChooseOther(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val datauser_list = jsonElement2.get("DataSupportIT").asJsonObject

                    try {

                        val resultEmployee = datauser_list.get("BoxUserRequest")
                        val employeeList = gson.fromJson(resultEmployee, UserRequestList::class.java)

                        val user: MutableList<String> = mutableListOf()
                        user.add(0, "กรุณาเลือกข้อมูล")

                        val datauser_array = employeeList.emp_name_th

                        user.add(datauser_array.toString())
                        val readOnlyView_user: kotlin.collections.List<String> = user

                        val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_user)
                        App_createuser.adapter = adapter
                        App_createuser.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                Toast.makeText(getActivity(), readOnlyView_user[position], Toast.LENGTH_SHORT).show()
                            }

                            override fun onNothingSelected(adapterView: AdapterView<*>) {

                            }
                        }

                        var i: Int = 0
                        for (a in readOnlyView_user) {

                            if(a.equals(emp_name)){
                                App_createuser.setSelection(i)
                            }
                            i++
                        }

                    }

                    catch (e: Exception) {

                        try {

                            val dao_user = gson.fromJson(datauser_list, DataSupportIT::class.java)

                            val user: MutableList<String> = mutableListOf()
                            user.add(0, "กรุณาเลือกข้อมูล")

                            val datauser_array = dao_user.BoxUserRequest

                            //Log.d("Anser_user", "item list = " + datauser_array.toString())

                            if (datauser_array != null) {
                                for (a in datauser_array) {

                                    user.add(a.emp_name_th.toString())
                                }
                            }

                            val readOnlyView_User: kotlin.collections.List<String> = user

                            val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_User)
                            App_createuser.adapter = adapter
                            App_createuser.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                    Toast.makeText(getActivity(), readOnlyView_User[position], Toast.LENGTH_SHORT).show()
                                }

                                override fun onNothingSelected(adapterView: AdapterView<*>) {

                                }
                            }

                            var i: Int = 0
                            for (a in readOnlyView_User) {

                                if(a.equals(emp_name)){
                                    App_createuser.setSelection(i)
                                }
                                i++
                            }
                        }

                        catch (e: Exception) {

                            val user: MutableList<String> = mutableListOf()
                            user.add(0, "กรุณาเลือกข้อมูล")
                            val readOnlyView_user: kotlin.collections.List<String> = user

                            val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_user)
                            App_createuser.adapter = adapter
                            App_createuser.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                                    Toast.makeText(getActivity(), readOnlyView_user[position], Toast.LENGTH_SHORT).show()
                                }

                                override fun onNothingSelected(adapterView: AdapterView<*>) {

                                }
                            }
                        }
                    }

                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })

    }

    fun select_location() {


        var dataEmployee = DataSupportIT()
        var _dataemployee = data_employeelist()
        _dataemployee.org_idx = 1
        dataEmployee.employee_list = arrayListOf(_dataemployee)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataEmployee)
        val jsonObject = JsonObject()
        //jsonObject.add("data_employee", jsonElement)
        jsonObject.add("data_employee", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("Go", "return code = " + jsonElement)

        var call = HttpManager.getInstance().service.getItServicesList_GetPlace(jsonIn)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataemployee_list = jsonElement2.get("data_employee").asJsonObject

                    val dao_employee = gson.fromJson(dataemployee_list, DataSupportIT::class.java)

                    //Log.d("Ser", "item list = " + returnVal.toString())

                    val location: MutableList<String> = mutableListOf()
                    location.add(0, "กรุณาเลือกข้อมูล")

                    val dataEmployee_array = dao_employee.employee_list

                    Log.d("Anser", "item list = " + dataEmployee_array.toString())

                    if (dataEmployee_array != null) {
                        for (i in dataEmployee_array) {

                            location.add(i.LocName.toString())
                        }
                    }



                    val readOnlyView_location: kotlin.collections.List<String> = location //listOf(dataSupportIT)


                    val adapter = ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, readOnlyView_location)
                    App_location.adapter = adapter
                    App_location.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(adapterView: AdapterView<*>, view: View, position: Int, l: Long) {
                            Toast.makeText(getActivity(), readOnlyView_location[position], Toast.LENGTH_SHORT).show()
                        }

                        override fun onNothingSelected(adapterView: AdapterView<*>) {

                        }
                    }

                    /*val returnCode = dataSupportIT.get("ReturnCode").asInt
                    Log.d("ServicesIT", "return code = " + returnCode.toString())

                    if (returnCode == 0) {
                        val dao = gson.fromJson(dataSupportIT, DataSupportIT::class.java)
                        Log.d("ServicesIT", "dao = " + dao.toString())
                        ItServicesListManager.getInstance().dao = dao
                        //listAdapter.notifyDataSetChanged()
                    } else {
                        // TODO : toast error
                    }*/
                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    fun Insert_Google() {


        var tel: String = et_tel.text.toString()
        var location: String = App_location.selectedItem.toString()
        var comment: String = et_comment.text.toString()


        var dataSupportIT = DataSupportIT()
        var userRequestList = UserRequestList()
        userRequestList.fileuser = 0
        userRequestList.empidx_add = 173 //ข้อมูลจากหน้า Login
        userRequestList.locname = location
        userRequestList.sysidx_add = 1
        userRequestList.iso_user = "1"
        userRequestList.teletc = tel
        userRequestList.emailetc = ""
        userRequestList.costidx = 0
        userRequestList.ncempidx = 173 //ข้อมูลจากหน้า Login

        userRequestList.adminidx = 0
        userRequestList.admindoingidx = 0
        userRequestList.commentamdoing = ""
        userRequestList.fileamdoing = 0
        userRequestList.ccaidx = 0
        userRequestList.staidx = 1
        userRequestList.emailidx = 1 // Defult User Stored Mobile
        userRequestList.detailuser = comment

        dataSupportIT.BoxUserRequest = arrayListOf(userRequestList)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        Log.d("DataSupportIT", "jsonIn = " + jsonIn)

        var call = HttpManager.getInstance().service.getItServicesList_Insert(jsonIn)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    Log.d("DataSupportIT", "success body = " + returnVal)

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupport = jsonElement2.get("DataSupportIT").asJsonObject

                } else {
                    Log.d("DataSupportIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("Employee", "failure = " + t.toString())
            }
        })
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.btnGoogleCreateSave -> {

                if( App_createuser.selectedItem.toString() == "กรุณาเลือกข้อมูล" || App_location.selectedItem.toString() == "กรุณาเลือกข้อมูล" ||
                        et_comment.text.toString() == ""){

                    val simpleAlert = AlertDialog.Builder(getActivity()).create()
                    simpleAlert.setTitle("แจ้งเตือน")
                    simpleAlert.setMessage("กรุณากรอกข้อมูลให้ครบถ้วน")

                    simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", { dialogInterface, i ->

                    })

                    simpleAlert.show()
                    Log.d("Insert", "No = ไม่สำเร็จ")

                }
                else{
                    Insert_Google()
                    Log.d("Insert", "OK = สำเร็จ")

                    var googleCreateSave = Intent(activity, ItGoogleListActivity::class.java)
                    startActivity(googleCreateSave)
                }


            }
            R.id.btnGoogleCreateCancel -> {
                var googleCreateCancel = Intent(activity, ItGoogleListActivity::class.java)
                startActivity(googleCreateCancel)
            }
        }
    }
}