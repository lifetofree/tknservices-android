package tkn.dev.tknservices.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItGoogleCreateActivity
import tkn.dev.tknservices.activity.ItGoogleDetailActivity
import tkn.dev.tknservices.adapter.ItServicesListAdapter
import tkn.dev.tknservices.dao.DataSupportIT
import tkn.dev.tknservices.dao.UserRequestList
import tkn.dev.tknservices.manager.Contextor
import tkn.dev.tknservices.manager.HttpManager
import tkn.dev.tknservices.manager.ItServicesListManager

/**
 * Created by lifetofree on 10/19/17.
 */
class ItGoogleListFragment : Fragment() {

    var someVar: Int = 0
    lateinit var listView: ListView
    lateinit var listAdapter: ItServicesListAdapter

    private lateinit var btnItGoogleList: Button
    private lateinit var btnItDetailList: Button
    private lateinit var btnItAdminllList: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_google_list, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItGoogleListFragment {
            val fragment = ItGoogleListFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    private fun initInstances(rootView: View) {

        //btnItGoogleList = rootView.findViewById(R.id.btnItGoogleList)
        //btnItDetailList = rootView.findViewById(R.id.btnItDetailList)
        //btnItAdminllList = rootView.findViewById(R.id.btnItAdminllList)

        //btnItGoogleList.setOnClickListener(listener)
        //btnItDetailList.setOnClickListener(listener)
        //btnItAdminllList.setOnClickListener(listener)

        listView = rootView.findViewById(R.id.listView_gmail)
        listAdapter = ItServicesListAdapter()
        listView.adapter = listAdapter

        select_listview()
    }

    private fun select_listview() {

        var dataSupportIT = DataSupportIT()
        var boxUserRequest = UserRequestList()
        boxUserRequest.sysidx_add = 1
        dataSupportIT.BoxUserRequest = arrayListOf(boxUserRequest)

        val gson = GsonBuilder().create()

        val jsonElement: JsonElement = gson.toJsonTree(dataSupportIT)
        val jsonObject = JsonObject()
        jsonObject.add("DataSupportIT", jsonElement)
        val jsonIn = jsonObject.toString()

        var call = HttpManager.getInstance().service.getItServicesList_mobile(jsonIn)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                if (response.isSuccessful) {
                    val returnVal = response.body()?.string()

                    val jsonElement2 = gson.fromJson(returnVal, JsonObject::class.java)
                    val dataSupportIT = jsonElement2.get("DataSupportIT").asJsonObject

                    val returnCode = dataSupportIT.get("ReturnCode").asInt
                    Log.d("ServicesIT", "return code = " + returnCode.toString())

                    if (returnCode == 0) {
                        // TODO: Check result is object or array
                        val dao = gson.fromJson(dataSupportIT, DataSupportIT::class.java)
                        Log.d("ServicesIT", "dao = " + dao.toString())
                        ItServicesListManager.getInstance().dao = dao
                        listAdapter.notifyDataSetChanged()

                        // Set onItemClickListener
                        listView.onItemClickListener = AdapterView.OnItemClickListener({ adapterView: AdapterView<*>?, view: View?, i: Int, l: Long ->
//                            Toast.makeText(activity,
//                                    "on click = " + dao.BoxUserRequest!![i].urqidx.toString(),
//                                    Toast.LENGTH_SHORT)
//                                    .show()
                            var selectedData: UserRequestList = dao.BoxUserRequest!![i]
                            val intent = Intent(activity, ItGoogleDetailActivity::class.java)
                            intent.putExtra("selectedData", selectedData)
                            startActivity(intent)
                        })
                    } else {
                        // TODO : toast error
                    }
                } else {
                    Log.d("ServicesIT", "error body = " + response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                Log.d("ServicesIT", "failure = " + t.toString())
            }
        })
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            /*R.id.btnItGoogleList -> {
                var googleCreate = Intent(activity, ItGoogleCreateActivity::class.java)
                startActivity(googleCreate)
            }
            R.id.btnItDetailList -> {
                var googleDetail = Intent(activity, ItGoogleDetailActivity::class.java)
                startActivity(googleDetail)
            }
            R.id.btnItAdminllList -> {
                val twoFragment = ItGoogleAdminFragment()
                val transaction = fragmentManager.beginTransaction()
                transaction.replace(R.id.contentContainer, twoFragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }*/
        }
    }
}