package tkn.dev.tknservices.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tkn.dev.tknservices.R
import tkn.dev.tknservices.activity.ItItListActivity

class ItPosCommentFragment : Fragment() {

    var someVar: Int = 0

    private lateinit var tvCustomTitleBar: TextView
    private lateinit var btnItPosList: Button
    private lateinit var btnItPosCancel: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read From Arguments
//        someVar = arguments.getInt("someVar")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var rootView: View = inflater!!.inflate(R.layout.fragment_it_pos_comment, container, false)
        initInstances(rootView)

        return rootView
    }

    companion object {
        fun newInstance(): ItPosCommentFragment {
            val fragment = ItPosCommentFragment()
//            var agrs = Bundle() // Arguments
//            agrs.putInt("someVar", someVar)
//            fragment.arguments = agrs
            return fragment
        }
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    private fun initInstances(rootView: View) {
        // Init 'View' instance(s) with rootView.findViewById here
        /* tvCustomTitleBar = rootView.findViewById(R.id.tvCustomTitleBar)
        tvCustomTitleBar.text = "it"
        tvCustomTitleBar.setBackgroundResource(R.drawable.bg_title_it_it) */

        //tvCustomTitleBar = activity.findViewById(R.id.tvCustomTitleBar)
        //tvCustomTitleBar.text = "It"

        //tvCustomTitleBar = rootView.findViewById(R.id.btnCenItIt)

        //tvCustomTitleBar.setOnClickListener(listener)
        // Init 'View' instance(s) with rootView.findViewById here
        //tvCustomTitleBar = activity.findViewById(R.id.tvCustomTitleBar)
        //tvCustomTitleBar.text = "IT Comment"
        //tvCustomTitleBar.setBackgroundResource(R.drawable.bg_title_it_it)

        btnItPosList = rootView.findViewById(R.id.btnItPosList)
        btnItPosCancel = rootView.findViewById(R.id.btnItPosCancel)

        btnItPosList.setOnClickListener(listener)
        btnItPosCancel.setOnClickListener(listener)
    }

    // Save Instance State Here
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        // Save Instance State Here
    }

    // Restore Instance State here
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    var listener: View.OnClickListener = View.OnClickListener { view ->
        when(view.id) {
            R.id.btnItPosList -> {
                var itItList = Intent(activity, ItPosListFragment::class.java)
                startActivity(itItList)
            }
            R.id.btnItPosCancel -> {
                var itItCancel = Intent(activity, ItPosListFragment::class.java)
                startActivity(itItCancel)
            }
        }
    }
}